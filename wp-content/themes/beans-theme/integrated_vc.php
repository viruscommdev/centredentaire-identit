<?php

/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/10/2017
 * Time: 10:55
 */
class basic_vc_module
{
    public $category = "viruscomm components";
    public $prefix_shortCode = "vsc_";
    public $list_of_modules = [];

    function __construct()
    {
        add_action('vc_before_init', array(&$this, "on_loaded"));
//        $this->addVcJson();
    }

    function on_loaded()
    {

        foreach ($this->list_of_modules as $module) {
            $newSlugName = $this->init_shortCode($module["slugName"], $module["fn"]);
            $this->vc_map_call($newSlugName, $module["name"], $module["params"], $module["extend"]);
        }
    }

    function vc_map_call($slugName, $name, $params, $extend = [])
    {
        $setting = array(
            "name" => __($name, "my-text-domain"),
            "base" => $slugName,
            "class" => "",
            "category" => __($this->category, "my-text-domain"),
            "params" => $params
        );
        $setting = array_merge($setting, $extend);
        vc_map($setting);
    }

    function all_in_one($slugName, $fn, $name, $params, $extend = [], $data = [])
    {
//        $content = call_user_func($fn, ["collecting-data" => "none"]);
//        $inputs = $this->getInputs($content);
        $this->list_of_modules[] = [
            "slugName" => $slugName,
            "fn" => $fn,
            "name" => $name,
            "params" => array_merge($params),
            "extend" => $extend
        ];
    }

    function init_shortCode($slugName, $fn)
    {

        add_shortcode($this->prefix_shortCode . $slugName, $fn);
        return $this->prefix_shortCode . $slugName;
    }

    function getDirContents($dir, &$results = [])
    {
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
            }
        }

        return $results;
    }
}
class catch_inputs extends basic_vc_module{

    function getInputs($content, $getRegex = false)
    {

        $result = [];
        if (strpos($content, "<!--endfield-->") !== false) {
            $content = explode("<!--endfield-->", $content);

            foreach ($content as $field) {
                $re = '/<!--({[^}]+})-->(.+)/s';
                preg_match_all($re, $field, $matches, PREG_PATTERN_ORDER);
                $result[] = $matches;
            }
        }

        if ($getRegex)
            return $result;
        else
            return $this->rematchInputs($result);
    }

    function rematchInputs($result)
    {
        $fields = [];
        foreach ($result as $input) {
            if (count($input[0]) > 0) {
                $value = $input[2][0];
                $data = json_decode($input[1][0], true);
                if ($data["name"] && $data["type"])
                    $fields[] = [
                        "type" => $data["type"],
                        "holder" => "div",
                        "class" => "",
                        "heading" => $data["title"] ? $data["title"] : $data["name"],
                        "param_name" => $data["name"],
                        "value" => $value
                    ];
            }
        }
        return $fields;
    }
    function rebuild($content, $atts)
    {
        if ($atts["collecting-data"])
            return $content;
        $inputs = $this->getInputs($content, true);
        foreach ($inputs as $input) {
            if (count($input[0]) > 0) {
                $defaultValue = $input[2][0];
                $data = json_decode($input[1][0], true);

                if ($data["name"] && $data["type"]) {
                    $value = $atts[$data["name"]];
                    switch ($data["type"]) {
                        case "textfield":
                            $content = str_replace($input[0][0], $value, $content);
                            break;
                        case "textarea":
                            $content = str_replace($input[0][0], $value, $content);
                            break;
                        default:
                            $content = str_replace($input[0][0], $defaultValue, $content);
                    }
                }
            }
        }
        return $content;
    }
}
class vc_module extends basic_vc_module{
    function readInput($filename)
    {
        $data = json_decode(file_get_contents($filename), true);

        $this->resultSector = [];
        $result = $this->sector($data["items"][0]);
        return $result;
    }

    function contentText($input, $textCount, $slug)
    {
        return [
            [
                "type" => "textarea_html",
                "holder" => "div",
                "class" => "",
                "heading" => "content",
                "param_name" => "content",
                "value" => str_replace("{{text.content}}", "", $input["value"]["text"]),
                "defValue" => $input["value"]["text"],
                "typeInput" => "content"
            ]
        ];
    }

    function RichText($input, $textCount, $slug = "text")
    {
        if ($input["skipElement"] === "1")
            return [];

        $re = '/{{([^}]+)}}/m';
        if (!array_key_exists("text", $input["value"]))
            return [];
        $str = $input["value"]["text"];

        if (strpos($str, "{{text.content}}") !== false) {
//            var_dump($this->contentText($input, $textCount, $slug));
//            wp_die();
            return $this->contentText($input, $textCount, $slug);
        }

        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
        if (count($matches) > 0) {
            $arr = [];
            foreach ($matches as $index => $match) {
                $arr[] = [
                    "type" => "textarea",
                    "holder" => "div",
                    "class" => "",
                    "heading" => $match[1],
                    "param_name" => $slug . "_" . $textCount . "_" . $index,
                    "value" => $match[1],
                    "defValue" => $match[0]
                ];
            }
            return $arr;
        } else {
            return [[
                "type" => "textarea_raw_html",
                "holder" => "div",
                "class" => "",
                "heading" => $slug . " " . $textCount,
                "param_name" => $slug . "_" . $textCount,
                "value" => base64_encode($input["value"]["text"])
            ]];
        }
    }

    function imageInput($input, $imgCount)
    {

        if ($input["skipElement"] === "1")
            return [];
        return [[
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => "image " . $imgCount,
            "param_name" => "image_" . $imgCount,
            "defValue" => $input["value"]["imageSrc"],
            "typeInput" => "image"
        ]];
    }

    function BgImageInput($input, $imgCount)
    {

        if ($input["skipElement"] === "1")
            return [];
        return [[
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => "background image " . $imgCount,
            "param_name" => "bgimage_" . $imgCount,
            "defValue" => $input["value"]["bgImageSrc"],
            "typeInput" => "bgimage"
        ]];
    }

    function tabInput($input, $tabCount, $slug = "tab")
    {
        if ($input["skipElement"] === "1")
            return [];

        $str = $input["value"]["labelText"];
        preg_match_all('/{{([^}]+)}}/m', $str, $matches, PREG_SET_ORDER, 0);
        if (count($matches) > 0) {
            $arr = [];
            foreach ($matches as $index => $match) {
                $arr[] = [
                    "type" => "textarea",
                    "holder" => "div",
                    "class" => "",
                    "heading" => $match[1],
                    "param_name" => $slug . "_" . $tabCount . "_" . $index,
                    "value" => $match[1],
                    "defValue" => $match[0]
                ];
            }
            return $arr;
        } else {
            return [];
        }
    }

    function linkInput($input, $linkCount, $slug = "link")
    {
        if ($input["skipElement"] === "1")
            return [];
        $str = $input["value"]["text"];
        preg_match_all('/{{([^}]+)}}/m', $str, $matches, PREG_SET_ORDER, 0);
        if (count($matches) > 0) {

            $arr = [
                "type" => "vc_link",
                "holder" => "div",
                "class" => "",
                "heading" => $matches[0][1],
                "param_name" => $slug . "_" . $linkCount . "_" . $index,
                "value" => $matches[0][1],
                "defValue" => $matches[0][0],
                "typeInput" => "link",
                "defValueLink" => ""
            ];
            if(array_key_exists("linkExternal",$input["value"]))
                $arr["defValueLink"] = $input["value"]["linkExternal"];

            return [$arr];
        } else {
            return [];
        }

    }

    function linkIconInput($input, $linkCount, $slug = "link"){
        if ($input["skipElement"] === "1")
            return [];
        if(!array_key_exists("linkExternal",$input["value"]))
            return [];
        $str = $input["value"]["linkExternal"];

        preg_match_all('/{{([^}]+)}}/m', $str, $matches, PREG_SET_ORDER, 0);
        if (count($matches) > 0) {

            $arr = [
                "type" => "vc_link",
                "holder" => "div",
                "class" => "",
                "heading" => $matches[0][1],
                "param_name" => $slug . "_" . $linkCount . "_" . $index,
                "value" => $matches[0][1],
                "defValue" => $matches[0][0],
                "typeInput" => "icon",
                "defValueLink" => $matches[0][0]
            ];

            return [$arr];
        } else {
            return [];
        }
    }

    function inputsJson($filename)
    {
        $data = $this->readInput($filename);
        $inputs = [];
        $textCount = 0;
        $btnCount = 0;
        $imgCount = 0;
        $bgCount = 0;
        $tabCount = 0;
        $iconCount=0;
        foreach ($data as $input) {
            switch ($input["type"]) {
                case "RichText":
                    $textCount++;
                    $inputs = array_merge($inputs, $this->RichText($input, $textCount));
                    break;
                case  "Button":
                    $btnCount++;
                    $inputs = array_merge($inputs, $this->linkInput($input, $btnCount, "btn"));
                case "Image":
                    $imgCount++;
                    $inputs = array_merge($inputs, $this->imageInput($input, $imgCount));
                    break;
                case "Tab":
                    $tabCount++;
                    $inputs = array_merge($inputs, $this->tabInput($input, $tabCount));
                    break;
                case "Icon":
                    $iconCount++;
                    $inputs=array_merge($inputs,$this->linkIconInput($input,$iconCount,"icon"));
                    break;
                default:
                    if (array_key_exists("bgImageSrc", $input["value"])) {
                        $bgCount++;
                        $inputs = array_merge($inputs, $this->BgImageInput($input, $bgCount));
                    }
                    break;
            }
        }
        return $inputs;
    }

    function sector($data, $skipElement = false)
    {
        if (array_key_exists("value", $data) && array_key_exists("items", $data["value"])) {
            if (array_key_exists("bgImageSrc", $data["value"]))
                $this->resultSector[] = array_merge($data, ["skipElement" => $skipElement ? "1" : "0"]);
            if (array_key_exists("labelText", $data["value"]))
                $this->resultSector[] = array_merge($data, ["skipElement" => $skipElement ? "1" : "0"]);
            foreach ($data["value"]["items"] as $item) {
                if (array_key_exists("value", $item) &&
                    array_key_exists("customClassName", $item["value"]) &&
                    $item["value"]["customClassName"] == "skipElement" ||
                    $skipElement)
                    $this->sector($item, true);
                else
                    $this->sector($item);
            }
            return $this->resultSector;
        } else
            $this->resultSector[] = array_merge($data, ["skipElement" => $skipElement ? "1" : "0"]);

    }



    function catchBasicData($data)
    {
        include_once ABSPATH . "wp-content/themes/beans-theme/rebuildCode/vendor/autoload.php";
        $domHead = new gymadarasz\xparser\XNode($data["blocks"]["head"]);
        $body = str_replace('<div class="brz-root__container brz-reset-all">', "", $data["blocks"]["body"]);
        $body = substr($body, 0, 0 - strlen("</div>"));
        $style = @$domHead->find('style', 0)->inner();
        return ["body" => $body, "style" => $style];
    }

    function textContent($body)
    {
        $data = explode('<div class="brz-rich-text">', $body);
        $text = "";
        foreach ($data as $item)
            if (strpos($item, "{{text.content}}") !== false) {
                $text = $item;
                break;
            }
        $text = substr($text, 0, strpos($text, "</div>"));
        return $text;
    }

    function block_body_fn($filename, $atts = [], $content = null)
    {
        $pathinfo = pathinfo($filename);
        $data = file_get_contents(ABSPATH . "wp-content/themes/beans-theme/vc_extend_data/" . $pathinfo["filename"] . "_body.json");
        $data = json_decode($data, true);
        $BasicData = $this->catchBasicData($data);
        $body = $BasicData["body"];
        $style = $BasicData["style"];
        $inputs = $this->inputsJson($filename);

        if ($content) {
            foreach ($inputs as $input) {
                $typeInput = array_key_exists("typeInput", $input) ? $input["typeInput"] : "text";
                if ($typeInput == "content") {
                    $text = $this->textContent($body);
                    $body = str_replace($text, $content, $body);
                }
            }
        }
        if (is_array($atts))
            foreach ($atts as $key => $attr) {
                $defaultVal = "";
                foreach ($inputs as $input)
                    if ($key == $input["param_name"]) {
                        $typeInput = array_key_exists("typeInput", $input) ? $input["typeInput"] : "text";
                        switch ($typeInput) {
                            case"text":
                                $defaultVal = $input["defValue"] ? $input["defValue"] : $input["value"];
                                if ($input["type"] == "textarea_raw_html")
                                    $body = str_replace(urldecode(base64_decode($defaultVal)), urldecode(base64_decode($attr)), $body);
                                if ($input["type"] != "textarea_raw_html")
                                    $body = str_replace($defaultVal, $attr, $body);
                                break;
                            case "image":
                                $re = '/srcset="([^"]+)"/m';
                                preg_match_all($re, $body, $matches, PREG_SET_ORDER, 0);
                                foreach ($matches as $match) {
                                    $body = str_replace($match[0], "", $body);
                                }
                                $re = '/src="([^"]+)' . $input["defValue"] . '"/m';
                                preg_match_all($re, $body, $matches, PREG_SET_ORDER, 0);
                                $value = wp_get_attachment_image_url($attr, "full");

                                foreach ($matches as $match) {
                                    $body = str_replace($match[0], 'src="' . $value . '"', $body);
                                }
                                break;
                            case "bgimage":
                                $re = '/background-image:url\(([^\)]+' . $input["defValue"] . ')\);/m';
                                $value = wp_get_attachment_image_url($attr, "full");
                                preg_match_all($re, $style, $matches, PREG_SET_ORDER, 0);
                                foreach ($matches as $match) {
                                    $style = str_replace($match[1], $value, $style);
                                }
                                break;
                            case "link":
                                $href = vc_build_link($attr);
                                if ($input["defValueLink"] != "")
                                {
                                    $body = str_replace($input["defValueLink"], $href['url'], $body);
                                }
                                $body = str_replace($input["defValue"], $href['title'], $body);
                                break;
                            case "icon":
                                $href = vc_build_link($attr);
                                if ($input["defValueLink"] != "")
                                {
                                    $body = str_replace($input["defValueLink"], $href['url'], $body);
                                }
                                break;
                        }

                    }
            }
        $body = $this->fixLinks($body);
        return "<style>" . $style . "</style>" . $body;
    }

    function fixLinks($body)
    {
        $re = '/\?brizy=template\/icons\/glyph\/([^#]+)#nc_icon/m';
        $link = "wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/";
        preg_match_all($re, $body, $matches, PREG_SET_ORDER, 0);
        foreach ($matches as $match) {
            $body = str_replace($match[0], $link . $match[1] . "#nc_icon", $body);
        }
        $re = '/\?brizy=template\/icons\/editor\/([^#]+)#nc_icon/m';
        $link = "wp-content/uploads/brizy/editor/1.0.41/template/icons/editor/";
        preg_match_all($re, $body, $matches, PREG_SET_ORDER, 0);
        foreach ($matches as $match) {
            $body = str_replace($match[0], $link . $match[1] . "#nc_icon", $body);
        }
        $body=str_replace("http://denti.local","",$body);
        return $body;
    }

    function addVcJson()
    {
        foreach ($this->getDirContents(ABSPATH . 'wp-content/themes/beans-theme/vc_extend_data/') as $filename) {
            if (strpos($filename, "_body.json") === false) {

                $pathinfo = pathinfo($filename);
                $bodyfile = ABSPATH . "wp-content/themes/beans-theme/vc_extend_data/" . $pathinfo["filename"] . "_body.json";
                if (file_exists($bodyfile)) {

                    eval('$fn=function ($atts, $content) { global $vc_module;return $vc_module->block_body_fn("' . $filename . '",$atts, $content);};');
                    $this->list_of_modules[] = [
                        "slugName" => $pathinfo["filename"],
                        "fn" => $fn,
                        "name" => $pathinfo["filename"],
                        "params" => $this->inputsJson($filename),
                        "extend" => []
                    ];
                }
            }
        }
    }
}

$vc_module = new basic_vc_module();
foreach ($vc_module->getDirContents(ABSPATH . 'wp-content/themes/beans-theme/vc_extend/') as $filename) {
    include_once $filename;
}