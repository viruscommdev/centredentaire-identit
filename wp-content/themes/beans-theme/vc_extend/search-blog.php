<?php
$vc_module->all_in_one("search_list",
    function ($atts, $content) {
        $atts = shortcode_atts([
                
        ], $atts);
		$search_term = (isset($_GET['s'])) ? $_GET['s'] : 0;
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        $args=[
			"s" => $search_term,
            'post_type'=>"post",
            "post_status"=>"publish",
            'posts_per_page' => 2,
            'paged'          => $paged
        ];
        if($paged!=1)
        {
            $args['posts_per_page']=2;
        }
        
        query_posts( $args);
        ob_start();
		
        ?>
        <div class="blogPage">
            <div class="list">
             <?php if (!have_posts()) {
                    ?>
                   Aucun résultat pour votre recherche
                    <?php
                }else{
					
				echo '<h1 class="searchTitle padding60yb"><span class="colorTheme">'.$search_term.'</span> - résultats de recherche</h1>';
					
				}?>

                <?php

                while (have_posts()) {
                  
                    the_post();
                    ?>
                    <div class="article">
                    <div class="entry-meta">
                <div class="dateArticle">
                    <div class="day font24"><?=get_the_date('d')?></div>
                    <div class="month font24"><?=get_the_date('M')?></div>
                </div>
           
                     <div class="titleArticle">
                              <h2 class="font30">  <?=get_the_title()?> </h2>
                              <p class='author font18 lato regular'>Publié par: IdentiT <span class="share">

                           
                        <a href="http://www.facebook.com/sharer.php?u=<?= get_permalink() ?>" target="_blank" class="font18 lato regular"><svg class="brz-icon-svg"><use xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/logo-fb-simple.svg#nc_icon"></use></svg></a>
                        <a href="https://twitter.com/intent/tweet/?url=<?= get_permalink() ?>" target="_blank" class="font18 lato regular"><svg class="brz-icon-svg"><use xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/logo-twitter.svg#nc_icon"></use></svg></a>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= get_permalink() ?>" target="_blank" class="font18 lato regular"><svg class="brz-icon-svg"><use xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/logo-linkedin.svg#nc_icon"></use></svg></a>

                    </span></p>
                            </div>
                             </div>
                        <div class="img">
                            <?= wp_get_attachment_image(get_post_thumbnail_id( ), "full") ?>
                        </div>
                        <div class="except font18 lato regular black">
                            <?=get_the_excerpt()?>
                        </div>
                        <div class="lireplus font18 lato underline">
                                <a href="<?=get_permalink()?>" class="default">Lire plus</a>
                            
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="paginationBlog" style="    height: 71px;">
                <?php
                the_posts_pagination( array(
                    'mid_size'  => 2,
                    'prev_text' => __( '', 'textdomain' ),
                    'next_text' => __( '', 'textdomain' ),
                ) );
                ?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    },
    "search list",
    [

    ]);