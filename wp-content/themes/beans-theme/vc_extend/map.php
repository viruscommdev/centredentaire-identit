<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/10/2017
 * Time: 11:23
 */
$vc_module->all_in_one(
    "map",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(
            'mapid' => 'map1',
            'color' => '#333',
            'icon' => '417',
            'height' => '400px',
            'lat-long' => '45.4496916,-73.8568443'
        ), $atts);
        ob_start();
        ?>
        <div id="<?= $atts['mapid'] ?>" style="width: 100%;height: <?= $atts['height'] ?>"></div>
        <script>

            addedMap = false;
            jQuery(document).scroll(function () {
                if (!addedMap && jQuery(document).scrollTop() > 100) {
                    addedMap = true;
                    directionsDisplay = null;
                    directionsService = null;
                    autocomplete=null;
                    mapJS = {
                        map: null,
                        pinHome: null,
                        init: function () {
                            var input = document.getElementById("addressInput");
                            autocomplete = new google.maps.places.Autocomplete(input);


                            autocomplete.addListener('place_changed', mapJS.place_changed);
                            directionsDisplay = new google.maps.DirectionsRenderer;
                            directionsService = new google.maps.DirectionsService;
                            directionsDisplay.setMap(mapJS.map);

                        },
                        calculateAndDisplayRoute: function (directionsService, directionsDisplay, home, clinique) {
                            var selectedMode = "DRIVING";
                            directionsService.route({
                                origin: home,  // Haight.
                                destination: clinique,  // Ocean Beach.
                                // Note that Javascript allows us to access the constant
                                // using square brackets and a string value as its
                                // "property."
                                travelMode: google.maps.TravelMode[selectedMode]
                            }, function (response, status) {
                                if (status == 'OK') {
                                    directionsDisplay.setDirections(response);
                                } else {
                                    window.alert('Directions request failed due to ' + status);
                                }
                            });
                        },
                        addressGeo: function (address) {
                            geocoder = new google.maps.Geocoder();
                            if (address !== "") {
                                geocoder.geocode({'address': address}, function (results, status) {
                                    if (status == 'OK') {
                                        position = results[0].geometry.location;
                                        mapJS.place_changed(position);
                                    } else {
                                        alert('Geocode was not successful for the following reason: ' + status);
                                    }
                                });
                            }
                        },
                        place_changed: function (position) {
                            if(typeof position === "undefined"){

                                var place = autocomplete.getPlace();
                                if (!place.geometry) {
                                    // User entered the name of a Place that was not suggested and
                                    // pressed the Enter key, or the Place Details request failed.
                                    window.alert("No details available for input: '" + place.name + "'");
                                    return;
                                }
                                position =place.geometry.location;
                            }
                            if (mapJS.pinHome)
                                mapJS.pinHome.setPosition(position);
                            else
                                mapJS.pinHome = new google.maps.Marker({
                                    position: position,
                                    map: mapJS.map
                                });
                            mapJS.calculateAndDisplayRoute(directionsService,directionsDisplay,position,new google.maps.LatLng(<?=$atts['lat-long']?>));
                        },
                        setup: function () {
                            var points = new google.maps.LatLng(<?=$atts['lat-long']?>);
                            var styles = [
                                {
                                    "stylers": [
                                        {"visibility": "on"},
                                        {"hue": "<?=$atts['color']?>"}
                                    ]
                                }
                            ];

                            var mapOptions1 = {
                                scrollwheel: false,
                                // How zoomed in you want the map to start at (always required)
                                zoom: 16,
                                center: points, // New York
                                // This is where you would paste any style found on Snazzy Maps.
                                styles: styles
                            };
                            // Create the Google Map using our element and options defined above
                            mapJS.map = new google.maps.Map(document.getElementById("<?=$atts['mapid']?>"), mapOptions1);
                            var rectangle = new google.maps.Rectangle();

                            // Let's also add a marker while we're at it
                            var marker = new google.maps.Marker({
                                position: points,
                                map: mapJS.map,
                                icon: '<?= wp_get_attachment_image_url($atts['icon'], "full") ?>'
                            });
                            marker.setAnimation(google.maps.Animation.BOUNCE);

                            marker.addListener('mouseover', function () {
                                marker.setAnimation(null);
                            });
                            marker.addListener('mouseout', function () {
                                marker.setAnimation(google.maps.Animation.BOUNCE);
                            });
                            mapJS.init();
                        }
                    };
                    jQuery.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyBiZUYaoYcd-tfJ4ZTnAO7pP-HYGanWnAM&libraries=places", mapJS.setup);
                }
            });
        </script>
        <?php
        return ob_get_clean();
    }
    , "map",
    [
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("latitude and longitude", "my-text-domain"),
            "param_name" => "lat-long",
            "value" => __("45.4496916,-73.8568443", "my-text-domain")
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("map id", "my-text-domain"),
            "param_name" => "mapid",
            "value" => __("map1", "my-text-domain")
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("height", "my-text-domain"),
            "param_name" => "height",
            "value" => __("400px", "my-text-domain")
        ),
        array(
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => __("color map", "my-text-domain"),
            "param_name" => "color",
            "value" => "#333"
        ),
        array(
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => __("icon", "my-text-domain"),
            "param_name" => "icon",
            "value" => __("0", "my-text-domain")
        )
    ]);