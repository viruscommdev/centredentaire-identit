<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */

$vc_module->all_in_one("footer",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>

        <section id="efzepixpwg" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-15g4p86">
                        <div class="brz-bg-media">
                            <div class="brz-bg-image"></div>
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-1cghdkq">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-n2fij5">
                                                    <div class="brz-columns css-19rf22h">
                                                        <div class="brz-bg brz-d-xs-flex css-ntnmvj">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-q2hqk1">
                                                                                <div class="brz-image css-3s6t0h">
                                                                                    <picture>
                                                                                        <source srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=179&iH=54&oX=0&oY=0&cW=179&cH=54/41737cbe5adf24a4794a1dcc581189f821ce8a2d.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=358&iH=108&oX=0&oY=0&cW=358&cH=108/41737cbe5adf24a4794a1dcc581189f821ce8a2d.png 2x"
                                                                                                media="(min-width: 992px)">
                                                                                        <img class="brz-img"
                                                                                             src="/wp-content/uploads/brizy/pages/1.0.41/2/iW=122&iH=37&oX=0&oY=0&cW=122&cH=37/41737cbe5adf24a4794a1dcc581189f821ce8a2d.png"
                                                                                             srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=122&iH=37&oX=0&oY=0&cW=122&cH=37/41737cbe5adf24a4794a1dcc581189f821ce8a2d.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=244&iH=74&oX=0&oY=0&cW=244&cH=74/41737cbe5adf24a4794a1dcc581189f821ce8a2d.png 2x">
                                                                                    </picture>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-1cad2o0">
                                                    <div class="brz-columns css-4py51v">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-lh-lg-1_1 brz-lh-im-1_6 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-fs-im-6 brz-fs-lg-24">
                                                                                        Vivez la difference &#xE0; notre
                                                                                        clinique</p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-tp-nhdtypzlzo"><span
                                                                                            style="color: rgb(0, 0, 0);">B&#xE9;n&#xE9;ficiez d&#x2019;un ensemble complet de services dentaires de haute qualit&#xE9; &#xE0; la fine pointe&#xA0;de la technologie, pr&#xE8;s de chez vous, &#xE0; Kirkland. Notre &#xE9;quipe bilingue de professionnels dentaires qualifi&#xE9;s et passionn&#xE9;s saura vous mettre &#xE0; l&#x2019;aise et vous proposer des services&#xA0;&#xE0; la mesure de vos attentes !&#xA0; &#xA0;</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-azr7cd">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-row__container">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-row css-1cad2o0">
                                                                                <div class="brz-columns css-3q36jq">
                                                                                    <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content">
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-image css-cfty7q">
                                                                                                                <picture>
                                                                                                                    <source srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=72&iH=72&oX=0&oY=0&cW=72&cH=72/f46a00ef0ae276602fafd4caaeb0a8a052eb4f3f.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=144&iH=144&oX=0&oY=0&cW=144&cH=144/f46a00ef0ae276602fafd4caaeb0a8a052eb4f3f.png 2x"
                                                                                                                            media="(min-width: 992px)">
                                                                                                                    <img class="brz-img"
                                                                                                                         src="/wp-content/uploads/brizy/pages/1.0.41/2/iW=400&iH=400&oX=0&oY=0&cW=400&cH=400/f46a00ef0ae276602fafd4caaeb0a8a052eb4f3f.png"
                                                                                                                         srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=400&iH=400&oX=0&oY=0&cW=400&cH=400/f46a00ef0ae276602fafd4caaeb0a8a052eb4f3f.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=800&iH=800&oX=0&oY=0&cW=800&cH=800/f46a00ef0ae276602fafd4caaeb0a8a052eb4f3f.png 2x">
                                                                                                                </picture>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="brz-columns css-t551vv">
                                                                                    <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content">
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1sifoqf">
                                                                                                            <div class="brz-rich-text">
                                                                                                                <p class="brz-lh-lg-1_4 brz-lh-im-1_6 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-400 brz-fw-im-300 brz-fs-lg-16 brz-fs-im-17">
                                                                                                                    <span style="color: rgb(0, 0, 0);">Montr&#xE9;al</span>
                                                                                                                </p>
                                                                                                                <p class="brz-lh-lg-1_4 brz-lh-im-1_6 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-400 brz-fw-im-300 brz-fs-lg-16 brz-fs-im-17">
                                                                                                                    <span style="color: rgb(0, 0, 0);">9 Heath Hampstead (QC) H3X 3K8</span>
                                                                                                                </p>
                                                                                                                <p class="brz-lh-lg-1_4 brz-lh-im-1_6 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-400 brz-fw-im-300 brz-fs-lg-16 brz-fs-im-17">
                                                                                                                    <span style="color: rgb(0, 0, 0);">T. 514.860.1400</span>
                                                                                                                </p>
                                                                                                                <p class="brz-lh-lg-1_4 brz-lh-im-1_6 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-400 brz-fw-im-300 brz-fs-lg-16 brz-fs-im-17">
                                                                                                                    <span style="color: rgb(0, 0, 0);">F. 514.355.3909</span>
                                                                                                                </p></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-row__container">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-row css-1cad2o0">
                                                                                <div class="brz-columns css-9k6jk5">
                                                                                    <div class="brz-bg brz-d-xs-flex css-1hm60nl">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content">
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-rich-text">
                                                                                                                <p class="brz-lh-lg-1_4 brz-lh-im-1_6 brz-fs-lg-14 brz-fs-im-15 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-700 brz-fw-im-700">
                                                                                                                    <span style="color: rgb(0, 0, 0);">SUIVEZ NOUS SUR</span>
                                                                                                                </p></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="brz-columns css-zoy3jq">
                                                                                    <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content">
                                                                                            <div class="brz-wrapper-clone css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex brz-flex-xs-wrap css-1aa05kl">
                                                                                                            <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                                                <div class="brz-icon__container">
                                                                                                                <span class="brz-span brz-icon css-zz85lb"><svg
                                                                                                                        class="brz-icon-svg"><use
                                                                                                                            xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/logo-fb-simple.svg#nc_icon"/></svg></span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                                                <div class="brz-icon__container">
                                                                                                                <span class="brz-span brz-icon css-zz85lb"><svg
                                                                                                                        class="brz-icon-svg"><use
                                                                                                                            xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/logo-twitter.svg#nc_icon"/></svg></span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                                                <div class="brz-icon__container">
                                                                                                                <span class="brz-span brz-icon css-zz85lb"><svg
                                                                                                                        class="brz-icon-svg"><use
                                                                                                                            xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/logo-linkedin.svg#nc_icon"/></svg></span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "footer",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);
