<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("slider",
    function ($atts, $content = null) {
        $atts = shortcode_atts([
            "cover" => "",
			"textcover"=>"",
            "cover2"=>"",
            "textcover2"=>"",
			"cover3"=>"",
            "textcover3"=>""
        ], $atts);
		$cover = explode(",", $atts["cover"]);
		$cover2 = explode(",", $atts["cover2"]);
		$cover3 = explode(",", $atts["cover3"]);
        ob_start();
        ?>
        
        <style type="text/css">
		.css-1xwvmrg > .brz-bg-media > .brz-bg-image, [data-css-1xwvmrg] > .brz-bg-media > .brz-bg-image {
    background-image: url('<?= wp_get_attachment_image_url($cover[0], "full") ?>');}
	.css-1xbwuqn > .brz-bg-media > .brz-bg-image, [data-css-1xbwuqn] > .brz-bg-media > .brz-bg-image {
    background-image: url('<?= wp_get_attachment_image_url($cover2[0], "full") ?>');}
	.css-18qc318 > .brz-bg-media > .brz-bg-image, [data-css-18qc318] > .brz-bg-media > .brz-bg-image {
    background-image: url('<?= wp_get_attachment_image_url($cover3[0], "full") ?>');}
		</style>

        <section id="wmdlcumnwx" class="brz-section css-10qcoa1">
            <div class="brz-slick-slider" data-arrows="true"
                 data-next-arrow="/wp-content/uploads/brizy/editor/1.0.41/template/icons/editor/right-arrow-thin.svg#nc_icon"
                 data-prev-arrow="/wp-content/uploads/brizy/editor/1.0.41/template/icons/editor/right-arrow-thin.svg#nc_icon"
                 data-dots="false" data-dots-class="brz-slick-slider__dots brz-slick-slider__dots--none" data-fade="false"
                 data-vertical="false" data-auto-play="false" data-auto-play-speed="3000">
                <div class="brz-section__items">
                    <div class="brz-section__content">
                        <div class="brz-bg css-1xwvmrg">
                            <div class="brz-bg-media">
                                <div class="brz-bg-image"></div>
                                
                            </div>
                            <div class="brz-bg-content">
                                <div class="brz-container__wrap css-b8uy3h">
                                    <div class="brz-container css-fo2o23">
                                        <div class="brz-wrapper css-6fvry1">
                                            <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-55m1q3">
                                                <div class="brz-bg-media">
                                                    <div class="brz-bg-color"></div>
                                                </div>
                                                <div class="brz-bg-content">
                                                    <div class="brz-d-xs-flex css-47lv8j">
                                                        <div class="brz-rich-text padding175xl"><p
                                                                class="brz-ls-lg-m_1_5 brz-fs-xs-34 brz-lh-xs-1_3 brz-text-lg-left brz-fs-lg-50 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-lh-lg-1_1" >
                                                                <span class="font50 white"><?=urldecode(base64_decode($atts["textcover"]))?></span>
                                                            </p>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="brz-section__items">
                    <div class="brz-section__content">
                        <div class="brz-bg css-1xbwuqn">
                            <div class="brz-bg-media">
                                <div class="brz-bg-image"></div>
                               
                            </div>
                            <div class="brz-bg-content">
                                <div class="brz-container__wrap css-b8uy3h">
                                    <div class="brz-container css-fo2o23">
                                        <div class="brz-wrapper css-6fvry1">
                                            <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-55m1q3">
                                                <div class="brz-bg-media">
                                                    <div class="brz-bg-color"></div>
                                                </div>
                                                <div class="brz-bg-content">
                                                    <div>
                                                        <div class="brz-rich-text padding222xr"><p
                                                                class="brz-ls-lg-m_1_5 brz-fs-xs-34 brz-lh-xs-1_3 flexEnd brz-fs-lg-50 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-lh-lg-1_1">
                                                                <span class="font50 white"><?=urldecode(base64_decode($atts["textcover2"]))?></span>
                                                            </p>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="brz-section__items">
                    <div class="brz-section__content">
                        <div class="brz-bg css-18qc318">
                            <div class="brz-bg-media">
                                <div class="brz-bg-image"></div>
                              
                            </div>
                            <div class="brz-bg-content">
                                <div class="brz-container__wrap css-b8uy3h">
                                    <div class="brz-container css-fo2o23">
                                        <div class="brz-wrapper css-6fvry1">
                                            <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-55m1q3">
                                                <div class="brz-bg-media">
                                                    <div class="brz-bg-color"></div>
                                                </div>
                                                <div class="brz-bg-content">
                                                    <div>
                                                        <div class="brz-rich-text padding108xr"><p
                                                                class="brz-ls-lg-m_1_5 brz-fs-xs-34 brz-lh-xs-1_3 flexEnd brz-fs-lg-50 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-lh-lg-1_1 ">
                                                                <span class="font50 white"><?=urldecode(base64_decode($atts["textcover3"]))?></span>
                                                            </p>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "slider",[
        [
            "type" => "attach_images",
            "holder" => "div",
            "class" => "",
            "heading" => __("image cover n°1", "my-text-domain"),
            "param_name" => "cover",
            "value" => __("", "my-text-domain")
        ],
			 [
            "type" => "textarea_raw_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("Texte du cover 1", "my-text-domain"),
            "param_name" => "textcover",
            "value" => __("", "my-text-domain")
        ],
			[
            "type" => "attach_images",
            "holder" => "div",
            "class" => "",
            "heading" => __("image cover n°2", "my-text-domain"),
            "param_name" => "cover2",
            "value" => __("", "my-text-domain")
        ],
		 [
            "type" => "textarea_raw_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("Texte du cover 2", "my-text-domain"),
            "param_name" => "textcover2",
            "value" => __("", "my-text-domain")
        ],
				[
            "type" => "attach_images",
            "holder" => "div",
            "class" => "",
            "heading" => __("image cover n°3", "my-text-domain"),
            "param_name" => "cover3",
            "value" => __("", "my-text-domain")
        ],
		 [
            "type" => "textarea_raw_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("Texte du cover 3", "my-text-domain"),
            "param_name" => "textcover3",
            "value" => __("", "my-text-domain")
        ]
    ]);