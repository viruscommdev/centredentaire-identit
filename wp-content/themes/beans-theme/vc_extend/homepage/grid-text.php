<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("gird-text",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>

        <section id="muexuvwmmt" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-nfy532">
                        <div class="brz-bg-media">
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-43882k">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-10jdpoz">
                                                    <div class="brz-columns css-nsvst2">
                                                        <div class="brz-bg brz-d-xs-flex css-85o7xj">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-row__container">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-row css-10jdpoz">
                                                                                <div class="brz-columns css-nsvst2">
                                                                                    <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content">
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-rich-text">
                                                                                                                <p class="brz-tp-ilxdvbvksk">
                                                                                                                    Nos
                                                                                                                    conseils
                                                                                                                    dentaires&#xA0;</p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-rich-text">
                                                                                                                <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3">
                                                                                                                    <span style="color: rgb(0, 0, 0);">Maintenez une bonne sant&#xE9;&#xA0;bucco-dentaire gr&#xE2;ce &#xE0; ces&#xA0;conseils et informations propos&#xE9;s par notre &#xE9;quipe de&#xA0;professionnels d&#xE9;vou&#xE9;s.&#xA0;</span>
                                                                                                                </p></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="brz-columns css-1v503i5">
                                                                                    <div class="brz-bg brz-d-xs-flex css-1igfuj5">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-image"></div>
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-row__container">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-row css-10jdpoz">
                                                                                <div class="brz-columns css-1v503i5">
                                                                                    <div class="brz-bg brz-d-xs-flex css-11v7a3c">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-image"></div>
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="brz-columns css-nsvst2">
                                                                                    <div class="brz-bg brz-d-xs-flex css-17oy4xx">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-video"
                                                                                                 data-mute="on"
                                                                                                 data-autoplay="on">
                                                                                                <iframe class="brz-iframe brz-bg-video__cover"
                                                                                                        style="display:none"></iframe>
                                                                                            </div>
                                                                                            <div class="brz-bg-map">
                                                                                                <iframe class="brz-iframe brz-bg-map__cover"
                                                                                                        style="display:none"></iframe>
                                                                                            </div>
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content">
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-rich-text">
                                                                                                                <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-fs-lg-22 brz-fs-im-21 brz-lh-lg-1_1 brz-lh-im-1">
                                                                                                                    <span style="color: rgb(255, 255, 255);">Les diff&#xE9;rentes&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-fs-lg-22 brz-fs-im-21 brz-lh-lg-1_1 brz-lh-im-1">
                                                                                                                    <span style="color: rgb(255, 255, 255);">disciplines de la&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-fs-lg-22 brz-fs-im-21 brz-lh-lg-1_1 brz-lh-im-1">
                                                                                                                    <span style="color: rgb(255, 255, 255);">dentisterie</span>
                                                                                                                </p></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-rich-text">
                                                                                                                <p class="brz-fw-im-300 brz-fw-lg-300 brz-ff-lato brz-ls-im-0 brz-ls-lg-0 brz-fs-lg-16 brz-fs-im-15 brz-lh-lg-1_4 brz-lh-im-1_3">
                                                                                                                    <span style="color: rgb(255, 255, 255);">Lorem ipsum dolor sit amet, id altera persequeris vim, mea an appareat prodesset. Cu mea error eripuit delicata. Nec soluta aliquid similique eu.</span>
                                                                                                                </p></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-nsvst2">
                                                        <div class="brz-bg brz-d-xs-flex css-85o7xj">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-row__container">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-row css-10jdpoz">
                                                                                <div class="brz-columns css-19rf22h">
                                                                                    <div class="brz-bg brz-d-xs-flex css-75d44z">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-video"
                                                                                                 data-mute="on"
                                                                                                 data-autoplay="on">
                                                                                                <iframe class="brz-iframe brz-bg-video__cover"
                                                                                                        style="display:none"></iframe>
                                                                                            </div>
                                                                                            <div class="brz-bg-map">
                                                                                                <iframe class="brz-iframe brz-bg-map__cover"
                                                                                                        style="display:none"></iframe>
                                                                                            </div>
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content">
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-rich-text">
                                                                                                                <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-fs-lg-22 brz-fs-im-21 brz-lh-lg-1_1 brz-lh-im-1">
                                                                                                                    <span style="color: rgb(255, 255, 255);">Les diff&#xE9;rentes&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-fs-lg-22 brz-fs-im-21 brz-lh-lg-1_1 brz-lh-im-1">
                                                                                                                    <span style="color: rgb(255, 255, 255);">disciplines de la&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-fs-lg-22 brz-fs-im-21 brz-lh-lg-1_1 brz-lh-im-1">
                                                                                                                    <span style="color: rgb(255, 255, 255);">dentisterie</span>
                                                                                                                </p></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-rich-text">
                                                                                                                <p class="brz-lh-im-1_3 brz-lh-lg-1_4 brz-fs-im-15 brz-fs-lg-16 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300">
                                                                                                                    <span style="color: rgb(255, 255, 255);">Lorem ipsum dolor sit amet, id altera&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-lh-im-1_3 brz-lh-lg-1_4 brz-fs-im-15 brz-fs-lg-16 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300">
                                                                                                                    <span style="color: rgb(255, 255, 255);">persequeris vim, mea an appareat&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-lh-im-1_3 brz-lh-lg-1_4 brz-fs-im-15 brz-fs-lg-16 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300">
                                                                                                                    <span style="color: rgb(255, 255, 255);">prodesset. Cu mea error eripuit delicata.&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-lh-im-1_3 brz-lh-lg-1_4 brz-fs-im-15 brz-fs-lg-16 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300">
                                                                                                                    <span style="color: rgb(255, 255, 255);">Nec soluta aliquid similique eu.</span>
                                                                                                                </p></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-row__container">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-row css-10jdpoz">
                                                                                <div class="brz-columns css-1v503i5">
                                                                                    <div class="brz-bg brz-d-xs-flex css-1pn5kr5">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-image"></div>
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="brz-columns css-nsvst2">
                                                                                    <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content">
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-rich-text">
                                                                                                                <p class="brz-lh-im-1 brz-lh-lg-1_1 brz-fs-im-21 brz-fs-lg-22 brz-fw-im-300 brz-fw-lg-300 brz-ff-lato brz-ls-im-0 brz-ls-lg-0">
                                                                                                                    <span style="color: rgb(5, 5, 5);">Les diff&#xE9;rentes&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-lh-im-1 brz-lh-lg-1_1 brz-fs-im-21 brz-fs-lg-22 brz-fw-im-300 brz-fw-lg-300 brz-ff-lato brz-ls-im-0 brz-ls-lg-0">
                                                                                                                    <span style="color: rgb(5, 5, 5);">disciplines de la&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-lh-im-1 brz-lh-lg-1_1 brz-fs-im-21 brz-fs-lg-22 brz-fw-im-300 brz-fw-lg-300 brz-ff-lato brz-ls-im-0 brz-ls-lg-0">
                                                                                                                    <span style="color: rgb(5, 5, 5);">dentisterie</span>
                                                                                                                </p></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-rich-text">
                                                                                                                <p class="brz-fw-im-300 brz-fw-lg-300 brz-ff-lato brz-ls-im-0 brz-ls-lg-0 brz-fs-lg-16 brz-fs-im-15 brz-lh-lg-1_4 brz-lh-im-1_3">
                                                                                                                    <span style="color: rgb(0, 0, 0);">Lorem ipsum dolor sit amet, id altera&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-fw-im-300 brz-fw-lg-300 brz-ff-lato brz-ls-im-0 brz-ls-lg-0 brz-fs-lg-16 brz-fs-im-15 brz-lh-lg-1_4 brz-lh-im-1_3">
                                                                                                                    <span style="color: rgb(0, 0, 0);">persequeris vim, mea an appareat&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-fw-im-300 brz-fw-lg-300 brz-ff-lato brz-ls-im-0 brz-ls-lg-0 brz-fs-lg-16 brz-fs-im-15 brz-lh-lg-1_4 brz-lh-im-1_3">
                                                                                                                    <span style="color: rgb(0, 0, 0);">prodesset. Cu mea error eripuit delicata.&#xA0;Nec soluta aliquid similique eu.</span>
                                                                                                                </p></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "gird-text",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);