<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("gallery3row",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>
        <section id="pdnrfapujo" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-ttiawf">
                        <div class="brz-bg-media">
                            <div class="brz-bg-video" data-mute="on" data-autoplay="on">
                                <iframe class="brz-iframe brz-bg-video__cover" style="display:none"></iframe>
                            </div>
                            <div class="brz-bg-map">
                                <iframe class="brz-iframe brz-bg-map__cover" style="display:none"></iframe>
                            </div>
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-1pzkdw1">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-10jdpoz">
                                                    <div class="brz-columns css-6w682k">
                                                        <div class="brz-bg brz-d-xs-flex css-eug6ik">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-image"></div>
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1trd8bt">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-spacer css-15m1g5h"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-2iv83d">
                                                        <div class="brz-bg brz-d-xs-flex css-1kdi9sb">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-image"></div>
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content"></div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-1wrc7dy">
                                                        <div class="brz-bg brz-d-xs-flex css-1tbzbvp">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-image"></div>
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "gallery3row",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);