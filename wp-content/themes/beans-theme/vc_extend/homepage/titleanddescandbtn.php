<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("titleanddescandbtn",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>

        <section id="niorvcnjgf" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-nfy532">
                        <div class="brz-bg-media">
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-1cs7zk9">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-14rabsn">
                                                    <div class="brz-columns css-19rf22h">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(0, 0, 0);">Une &#xE9;quipe exp&#xE9;riment&#xE9;e</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_5 brz-lh-im-1_4 brz-fw-lg-400 brz-fw-im-400">
                                                                                        <span style="color: rgb(0, 0, 0);">Pr&#xE9;sente depuis &#xE0; Kirkland depuis plus de 20 ans, l&#x2019;&#xE9;quipe du</span><strong
                                                                                            style="color: rgb(0, 0, 0);">
                                                                                            Centre dentaire
                                                                                            Identi-T&#xA0;</strong><span
                                                                                            style="color: rgb(0, 0, 0);">vous accueille&#xA0;dans une ambiance chaleureuse. Nos professionnels passionn&#xE9;s vous proposent des soins attentionn&#xE9;s&#xA0;tout en douceur.&#xA0;</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-lh-lg-1_7 brz-lh-im-1_6 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-25 brz-fs-im-24 brz-fw-lg-400 brz-fw-im-400">
                                                                                        <em style="color: rgb(0, 0, 0);">Ensemble
                                                                                            pour offrir
                                                                                            l&#x2019;excellence</em></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper-clone css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-i9tzew">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex brz-flex-xs-wrap css-1aa05kl">
                                                                                <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                    <a class="brz-a brz-btn css-e2llf"
                                                                                       href=""><span
                                                                                            class="brz-span brz-text__editor"
                                                                                            contenteditable="false">NOTRE &#xC9;QUIPE</span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "titleanddescandbtn",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);