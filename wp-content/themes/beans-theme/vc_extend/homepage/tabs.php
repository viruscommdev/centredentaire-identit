<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("tabs",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>

        <section id="respmxxtyx" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-nfy532">
                        <div class="brz-bg-media">
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-1e3wxl3">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-1cad2o0">
                                                    <div class="brz-columns css-19rf22h">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1 tabsHome">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-tabs css-1063fyz">
                                                                                    <ul class="brz-tabs__nav">
                                                                                        <li class="brz-tabs__nav--item brz-tabs__nav--desktop brz-tabs__nav--active">
                                                                                            <div class="brz-tabs__nav--button">
                                                                                            <span class="brz-span brz-text__editor"
                                                                                                  contenteditable="false">Dentisterie Esth&#xE9;tique</span>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li class="brz-tabs__nav--item brz-tabs__nav--desktop">
                                                                                            <div class="brz-tabs__nav--button">
                                                                                            <span class="brz-span brz-text__editor"
                                                                                                  contenteditable="false">Orthodontie</span>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li class="brz-tabs__nav--item brz-tabs__nav--desktop">
                                                                                            <div class="brz-tabs__nav--button">
                                                                                            <span class="brz-span brz-text__editor"
                                                                                                  contenteditable="false">Implantologie</span>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li class="brz-tabs__nav--item brz-tabs__nav--desktop">
                                                                                            <div class="brz-tabs__nav--button">
                                                                                            <span class="brz-span brz-text__editor"
                                                                                                  contenteditable="false">Parodontie</span>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li class="brz-tabs__nav--item brz-tabs__nav--desktop">
                                                                                            <div class="brz-tabs__nav--button">
                                                                                            <span class="brz-span brz-text__editor"
                                                                                                  contenteditable="false">Endodontie</span>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li class="brz-tabs__nav--item brz-tabs__nav--desktop">
                                                                                            <div class="brz-tabs__nav--button">
                                                                                            <span class="brz-span brz-text__editor"
                                                                                                  contenteditable="false">Dentisterie G&#xE9;n&#xE9;rale</span>
                                                                                            </div>
                                                                                        </li>
                                                                                    </ul>
                                                                                    <div class="brz-tabs__content">
                                                                                        <div class="brz-tabs__items brz-tabs__items--active">
                                                                                            <div class="brz-tabs__nav--mobile brz-tabs__nav--mobile--active">
                                                                                                <div class="brz-tabs__nav--button">
                                                                                                    Dentisterie Esth&#xE9;tique
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="brz-tabs__item--content">
                                                                                                <div class="brz-bg">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-wrapper css-6fvry1">
                                                                                                            <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                                <div class="brz-bg-media">
                                                                                                                    <div class="brz-bg-color"></div>
                                                                                                                </div>
                                                                                                                <div class="brz-bg-content">
                                                                                                                    <div class="brz-d-xs-flex css-19s7pht">
                                                                                                                        <div class="brz-rich-text">
                                                                                                                            <p class="brz-tp-ilxdvbvksk brz-text-lg-center">
                                                                                                                                <span style="color: rgb(0, 0, 0);">Notre clinique dentaire &#xE0; Kirkland&#xA0;</span>
                                                                                                                            </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="brz-wrapper css-6fvry1">
                                                                                                            <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                                <div class="brz-bg-media">
                                                                                                                    <div class="brz-bg-color"></div>
                                                                                                                </div>
                                                                                                                <div class="brz-bg-content">
                                                                                                                    <div class="brz-d-xs-flex css-toohv1">
                                                                                                                        <div class="brz-rich-text">
                                                                                                                            <p class="brz-text-lg-center brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-400 brz-fw-im-400">
                                                                                                                                <span style="color: rgb(0, 0, 0);">Nous sommes heureux de vous accueillir &#xE0; notre clinique de Kirkland o&#xF9; nous offrons un &#xE9;ventail&#xA0;de soins dentaires afin de r&#xE9;pondre &#xE0; tous vos besoins et ceux de votre famille. Forts de leur exp&#xE9;rience combin&#xE9;e&#xA0;de plusieurs dizaines d&#x2019;ann&#xE9;es, les Drs Phan, Nguyen et Queiros sauront mettre leur passion, leur expertise&#xA0;</span>
                                                                                                                            </p>
                                                                                                                            <p class="brz-text-lg-center brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-400 brz-fw-im-400">
                                                                                                                                <span style="color: rgb(0, 0, 0);">et leur souci du travail bien fait &#xE0; votre service.</span>
                                                                                                                            </p>
                                                                                                                            <p class="brz-text-lg-center brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-400 brz-fw-im-400">
                                                                                                                                <span style="color: rgb(0, 0, 0);">&#xA0;</span>
                                                                                                                            </p>
                                                                                                                            <p class="brz-text-lg-center brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-400 brz-fw-im-400">
                                                                                                                                <span style="color: rgb(0, 0, 0);">&#xA0;Gr&#xE2;ce &#xE0; leurs solides formations en dentisterie g&#xE9;n&#xE9;rale et &#xE0; l&#x2019;assistance attentionn&#xE9;e de notre &#xE9;quipe bilingue&#xA0;de professionnels dynamiques, nous seront en mesure de nous occuper des diverses parties de votre bouche&#xA0;et de leur redonner leurs pleines capacit&#xE9;s fonctionnelles et esth&#xE9;tiques. Que ce soit pour un suivi pr&#xE9;ventif, pour&#xA0;une r&#xE9;paration dentaire, pour des soins des gencives ou des autres tissus de la bouche, pour un traitement esth&#xE9;tique&#xA0;ou encore pour remplacer une ou des dents manquantes, notre &#xE9;quipe saura r&#xE9;pondre &#xE0; vos attentes.&#xA0;Soyez assur&#xE9;s d&#x2019;un service personnalis&#xE9; et adapt&#xE9; &#xE0; vos besoins ; quels que soient votre &#xE2;ge et votre condition&#xA0;bucco-dentaire, nous serons en mesure de r&#xE9;pondre &#xE0; vos interrogations et r&#xE9;gler tous vos probl&#xE8;mes dans une ambiance chaleureuse et s&#xE9;curisante !</span>
                                                                                                                            </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="brz-wrapper-clone css-6fvry1">
                                                                                                            <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-i9tzew">
                                                                                                                <div class="brz-bg-media">
                                                                                                                    <div class="brz-bg-color"></div>
                                                                                                                </div>
                                                                                                                <div class="brz-bg-content">
                                                                                                                    <div class="brz-d-xs-flex brz-flex-xs-wrap css-2v55jk">
                                                                                                                        <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                                                            <a class="brz-a brz-btn css-15vm4k4"
                                                                                                                               href=""><span
                                                                                                                                    class="brz-span brz-text__editor"
                                                                                                                                    contenteditable="false">OBTENEZ UN RENDEZ-VOUS D&#xC8;S AUJOURD&#x2019;HUI !</span></a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="brz-tabs__items">
                                                                                            <div class="brz-tabs__nav--mobile">
                                                                                                <div class="brz-tabs__nav--button">
                                                                                                    Orthodontie
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="brz-tabs__item--content">
                                                                                                <div class="brz-bg">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-wrapper-clone css-6fvry1">
                                                                                                            <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                                <div class="brz-bg-media">
                                                                                                                    <div class="brz-bg-color"></div>
                                                                                                                </div>
                                                                                                                <div class="brz-bg-content">
                                                                                                                    <div class="brz-d-xs-flex brz-flex-xs-wrap css-2v55jk">
                                                                                                                        <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                                                            <a class="brz-a brz-btn css-1cpdk88"
                                                                                                                               href=""
                                                                                                                               target="_blank">
                                                                                                                                <svg class="brz-icon-svg css-1jc60k4">
                                                                                                                                    <use xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/tail-right.svg#nc_icon"/>
                                                                                                                                </svg>
                                                                                                                                <span class="brz-span brz-text__editor"
                                                                                                                                      contenteditable="false">BUTTON</span></a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="brz-tabs__items">
                                                                                            <div class="brz-tabs__nav--mobile">
                                                                                                <div class="brz-tabs__nav--button">
                                                                                                    Implantologie
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="brz-tabs__item--content">
                                                                                                <div class="brz-bg">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-wrapper-clone css-6fvry1">
                                                                                                            <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                                <div class="brz-bg-media">
                                                                                                                    <div class="brz-bg-color"></div>
                                                                                                                </div>
                                                                                                                <div class="brz-bg-content">
                                                                                                                    <div class="brz-d-xs-flex brz-flex-xs-wrap css-2v55jk">
                                                                                                                        <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                                                            <a class="brz-a brz-btn css-1cpdk88"
                                                                                                                               href=""
                                                                                                                               target="_blank">
                                                                                                                                <svg class="brz-icon-svg css-1jc60k4">
                                                                                                                                    <use xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/tail-right.svg#nc_icon"/>
                                                                                                                                </svg>
                                                                                                                                <span class="brz-span brz-text__editor"
                                                                                                                                      contenteditable="false">BUTTON</span></a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="brz-tabs__items">
                                                                                            <div class="brz-tabs__nav--mobile">
                                                                                                <div class="brz-tabs__nav--button">
                                                                                                    Parodontie
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="brz-tabs__item--content">
                                                                                                <div class="brz-bg">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-wrapper-clone css-6fvry1">
                                                                                                            <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                                <div class="brz-bg-media">
                                                                                                                    <div class="brz-bg-color"></div>
                                                                                                                </div>
                                                                                                                <div class="brz-bg-content">
                                                                                                                    <div class="brz-d-xs-flex brz-flex-xs-wrap css-2v55jk">
                                                                                                                        <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                                                            <a class="brz-a brz-btn css-1cpdk88"
                                                                                                                               href=""
                                                                                                                               target="_blank">
                                                                                                                                <svg class="brz-icon-svg css-1jc60k4">
                                                                                                                                    <use xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/tail-right.svg#nc_icon"/>
                                                                                                                                </svg>
                                                                                                                                <span class="brz-span brz-text__editor"
                                                                                                                                      contenteditable="false">BUTTON</span></a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="brz-tabs__items">
                                                                                            <div class="brz-tabs__nav--mobile">
                                                                                                <div class="brz-tabs__nav--button">
                                                                                                    Endodontie
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="brz-tabs__item--content">
                                                                                                <div class="brz-bg">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-wrapper-clone css-6fvry1">
                                                                                                            <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                                <div class="brz-bg-media">
                                                                                                                    <div class="brz-bg-color"></div>
                                                                                                                </div>
                                                                                                                <div class="brz-bg-content">
                                                                                                                    <div class="brz-d-xs-flex brz-flex-xs-wrap css-2v55jk">
                                                                                                                        <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                                                            <a class="brz-a brz-btn css-1cpdk88"
                                                                                                                               href=""
                                                                                                                               target="_blank">
                                                                                                                                <svg class="brz-icon-svg css-1jc60k4">
                                                                                                                                    <use xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/tail-right.svg#nc_icon"/>
                                                                                                                                </svg>
                                                                                                                                <span class="brz-span brz-text__editor"
                                                                                                                                      contenteditable="false">BUTTON</span></a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="brz-tabs__items">
                                                                                            <div class="brz-tabs__nav--mobile">
                                                                                                <div class="brz-tabs__nav--button">
                                                                                                    Dentisterie G&#xE9;n&#xE9;rale
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="brz-tabs__item--content">
                                                                                                <div class="brz-bg">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-wrapper-clone css-6fvry1">
                                                                                                            <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                                <div class="brz-bg-media">
                                                                                                                    <div class="brz-bg-color"></div>
                                                                                                                </div>
                                                                                                                <div class="brz-bg-content">
                                                                                                                    <div class="brz-d-xs-flex brz-flex-xs-wrap css-2v55jk">
                                                                                                                        <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                                                            <a class="brz-a brz-btn css-1cpdk88"
                                                                                                                               href=""
                                                                                                                               target="_blank">
                                                                                                                                <svg class="brz-icon-svg css-1jc60k4">
                                                                                                                                    <use xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/tail-right.svg#nc_icon"/>
                                                                                                                                </svg>
                                                                                                                                <span class="brz-span brz-text__editor"
                                                                                                                                      contenteditable="false">BUTTON</span></a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "tabs",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);