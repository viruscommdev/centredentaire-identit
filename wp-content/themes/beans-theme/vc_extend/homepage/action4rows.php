<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("action4rows",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>

        <section id="metezsetgi" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-1ln9jwx">
                        <div class="brz-bg-media">
                            <div class="brz-bg-video" data-mute="on" data-autoplay="on">
                                <iframe class="brz-iframe brz-bg-video__cover" style="display:none"></iframe>
                            </div>
                            <div class="brz-bg-map">
                                <iframe class="brz-iframe brz-bg-map__cover" style="display:none"></iframe>
                            </div>
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-14tzvb4">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-1cad2o0">
                                                    <div class="brz-columns css-1ivqk4u">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-image css-2akak2">
                                                                                    <picture>
                                                                                        <source srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=93&iH=93&oX=0&oY=0&cW=93&cH=93/cb046601fd519f172fc74ecc664b470ba3c10a8f.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=186&iH=186&oX=0&oY=0&cW=186&cH=186/cb046601fd519f172fc74ecc664b470ba3c10a8f.png 2x"
                                                                                                media="(min-width: 992px)">
                                                                                        <img class="brz-img"
                                                                                             src="/wp-content/uploads/brizy/pages/1.0.41/2/iW=144&iH=144&oX=0&oY=0&cW=144&cH=144/cb046601fd519f172fc74ecc664b470ba3c10a8f.png"
                                                                                             srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=144&iH=144&oX=0&oY=0&cW=144&cH=144/cb046601fd519f172fc74ecc664b470ba3c10a8f.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=288&iH=288&oX=0&oY=0&cW=288&cH=288/cb046601fd519f172fc74ecc664b470ba3c10a8f.png 2x">
                                                                                    </picture>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-text-lg-center brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-700 brz-fw-im-700">
                                                                                        <span style="color: rgb(255, 183, 27);">CLAVARDAGE EN DIRECT</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-1ivqk4u">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-image css-2akak2">
                                                                                    <picture>
                                                                                        <source srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=93&iH=93&oX=0&oY=0&cW=93&cH=93/a05d97e1c087d26aaba0b2f9297e29ab06d7b6e4.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=186&iH=186&oX=0&oY=0&cW=186&cH=186/a05d97e1c087d26aaba0b2f9297e29ab06d7b6e4.png 2x"
                                                                                                media="(min-width: 992px)">
                                                                                        <img class="brz-img"
                                                                                             src="/wp-content/uploads/brizy/pages/1.0.41/2/iW=144&iH=144&oX=0&oY=0&cW=144&cH=144/a05d97e1c087d26aaba0b2f9297e29ab06d7b6e4.png"
                                                                                             srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=144&iH=144&oX=0&oY=0&cW=144&cH=144/a05d97e1c087d26aaba0b2f9297e29ab06d7b6e4.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=288&iH=288&oX=0&oY=0&cW=288&cH=288/a05d97e1c087d26aaba0b2f9297e29ab06d7b6e4.png 2x">
                                                                                    </picture>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-text-lg-center brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-700 brz-fw-im-700">
                                                                                        <span style="color: rgb(255, 183, 27);">514. 000.0000</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-1ivqk4u">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-image css-2akak2">
                                                                                    <picture>
                                                                                        <source srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=93&iH=93&oX=0&oY=0&cW=93&cH=93/a9912e8ad8d1e0560fa23c9b5f6a00d6b64a3e71.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=186&iH=186&oX=0&oY=0&cW=186&cH=186/a9912e8ad8d1e0560fa23c9b5f6a00d6b64a3e71.png 2x"
                                                                                                media="(min-width: 992px)">
                                                                                        <img class="brz-img"
                                                                                             src="/wp-content/uploads/brizy/pages/1.0.41/2/iW=144&iH=144&oX=0&oY=0&cW=144&cH=144/a9912e8ad8d1e0560fa23c9b5f6a00d6b64a3e71.png"
                                                                                             srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=144&iH=144&oX=0&oY=0&cW=144&cH=144/a9912e8ad8d1e0560fa23c9b5f6a00d6b64a3e71.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=288&iH=288&oX=0&oY=0&cW=288&cH=288/a9912e8ad8d1e0560fa23c9b5f6a00d6b64a3e71.png 2x">
                                                                                    </picture>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-text-lg-center brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-700 brz-fw-im-700">
                                                                                        <span style="color: rgb(255, 183, 27);">TROUVER LA CLINIQUE</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-1ivqk4u">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-image css-2akak2">
                                                                                    <picture>
                                                                                        <source srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=93&iH=93&oX=0&oY=0&cW=93&cH=93/ea98d97c2113b14a3b2fd6097894d60366fd1a42.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=186&iH=186&oX=0&oY=0&cW=186&cH=186/ea98d97c2113b14a3b2fd6097894d60366fd1a42.png 2x"
                                                                                                media="(min-width: 992px)">
                                                                                        <img class="brz-img"
                                                                                             src="/wp-content/uploads/brizy/pages/1.0.41/2/iW=144&iH=144&oX=0&oY=0&cW=144&cH=144/ea98d97c2113b14a3b2fd6097894d60366fd1a42.png"
                                                                                             srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=144&iH=144&oX=0&oY=0&cW=144&cH=144/ea98d97c2113b14a3b2fd6097894d60366fd1a42.png 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=288&iH=288&oX=0&oY=0&cW=288&cH=288/ea98d97c2113b14a3b2fd6097894d60366fd1a42.png 2x">
                                                                                    </picture>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-fw-im-700 brz-fw-lg-700 brz-lh-im-1_3 brz-lh-lg-1_4 brz-fs-im-17 brz-fs-lg-18 brz-ff-lato brz-ls-im-0 brz-ls-lg-0 brz-text-lg-center">
                                                                                        <span style="color: rgb(255, 183, 27);">M&#xC9;DIAS SOCIAUX</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "action4rows",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);