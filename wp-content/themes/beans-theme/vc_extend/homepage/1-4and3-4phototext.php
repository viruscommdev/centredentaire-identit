<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("1-4and3-4phototext",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>
        <section id="iexrdxprmb" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-nfy532">
                        <div class="brz-bg-media">
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-c7obq1">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-10jdpoz">
                                                    <div class="brz-columns css-ol0y3e">
                                                        <div class="brz-bg brz-d-xs-flex css-cyi6gu">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-video" data-mute="on" data-autoplay="on">
                                                                    <iframe class="brz-iframe brz-bg-video__cover"
                                                                            style="display:none"></iframe>
                                                                </div>
                                                                <div class="brz-bg-map">
                                                                    <iframe class="brz-iframe brz-bg-map__cover"
                                                                            style="display:none"></iframe>
                                                                </div>
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(0, 0, 0);">Mode&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(0, 0, 0);">de paiement</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-6 brz-lh-lg-1_4 brz-lh-im-1_9 brz-fw-lg-400 brz-fw-im-400">
                                                                                        <span style="color: rgb(0, 0, 0);">Nous discuterons alors du plan&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-6 brz-lh-lg-1_4 brz-lh-im-1_9 brz-fw-lg-400 brz-fw-im-400">
                                                                                        <span style="color: rgb(0, 0, 0);">de paiement qui vous convient le mieux de fa&#xE7;on discr&#xE8;te et confidentielle en plus de vous&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-6 brz-lh-lg-1_4 brz-lh-im-1_9 brz-fw-lg-400 brz-fw-im-400">
                                                                                        <span style="color: rgb(0, 0, 0);">assistez avec toutes les r&#xE9;clamations.</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper-clone css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-i9tzew">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex brz-flex-xs-wrap css-1aa05kl">
                                                                                <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                    <a class="brz-a brz-btn css-e2llf"
                                                                                       href=""><span
                                                                                            class="brz-span brz-text__editor"
                                                                                            contenteditable="false">EN SAVOIR PLUS</span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-1061yym">
                                                        <div class="brz-bg brz-d-xs-flex css-1n6ueah">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-image"></div>
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(0, 0, 0);">Votre sourire&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(0, 0, 0);">est notre&#xA0; priorit&#xE9;</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-6 brz-fw-lg-400 brz-fw-im-400 brz-lh-lg-1_4 brz-lh-im-1_9">
                                                                                        Pour notre &#xE9;quipe de
                                                                                        professionnels&#xA0;</p>
                                                                                    <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-6 brz-fw-lg-400 brz-fw-im-400 brz-lh-lg-1_4 brz-lh-im-1_9">
                                                                                        dentaires, form&#xE9;e de trois
                                                                                        dentistes&#xA0;</p>
                                                                                    <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-6 brz-fw-lg-400 brz-fw-im-400 brz-lh-lg-1_4 brz-lh-im-1_9">
                                                                                        g&#xE9;n&#xE9;ralistes passionn&#xE9;s,
                                                                                        d&#x2019;hygi&#xE9;nistes
                                                                                        et&#xA0;</p>
                                                                                    <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-6 brz-fw-lg-400 brz-fw-im-400 brz-lh-lg-1_4 brz-lh-im-1_9">
                                                                                        d&#x2019;assistantes dentaires
                                                                                        certifi&#xE9;es et&#xA0;</p>
                                                                                    <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-6 brz-fw-lg-400 brz-fw-im-400 brz-lh-lg-1_4 brz-lh-im-1_9">
                                                                                        d&#xE9;vou&#xE9;es, votre sant&#xE9;
                                                                                        bucco-dentaire est&#xA0;</p>
                                                                                    <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-6 brz-fw-lg-400 brz-fw-im-400 brz-lh-lg-1_4 brz-lh-im-1_9">
                                                                                        notre priorit&#xE9;.&#xA0;</p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1l144ec">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-fs-im-7 brz-fs-lg-25 brz-fw-im-300 brz-fw-lg-300 brz-ff-lato brz-ls-im-0 brz-ls-lg-0 brz-lh-im-1_6 brz-lh-lg-1_1">
                                                                                        Nous vous donnons le
                                                                                        go&#xFB;t&#xA0;</p>
                                                                                    <p class="brz-fs-im-7 brz-fs-lg-25 brz-fw-im-300 brz-fw-lg-300 brz-ff-lato brz-ls-im-0 brz-ls-lg-0 brz-lh-im-1_6 brz-lh-lg-1_1">
                                                                                        de sourire !&#xA0;&#xA0;</p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "1-4and3-4phototext",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);