<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:19 PM
 */
$vc_module->all_in_one("footer-copyright",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>

        <section id="ovjntaupzf" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-1ln9jwx">
                        <div class="brz-bg-media">
                            <div class="brz-bg-video" data-mute="on" data-autoplay="on">
                                <iframe class="brz-iframe brz-bg-video__cover" style="display:none"></iframe>
                            </div>
                            <div class="brz-bg-map">
                                <iframe class="brz-iframe brz-bg-map__cover" style="display:none"></iframe>
                            </div>
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-c7obq1">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-1cad2o0">
                                                    <div class="brz-columns css-19rf22h">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-tp-nhdtypzlzo"><span
                                                                                            style="color: rgb(255, 255, 255);">2018 Identi-T. All rights reserved.</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "footer-copyright",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "copyright"
        )
    ]);
