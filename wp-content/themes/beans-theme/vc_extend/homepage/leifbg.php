<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("leifbg",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>

        <section id="ndeqdfcbzr" class="brz-section css-lfbrd7 bgLeif">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-nfy532">
                        <div class="brz-bg-media">
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-1tr7msa">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-1cad2o0">
                                                    <div class="brz-columns css-u8ahs9">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-row__container">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-row css-1cad2o0">
                                                                                <div class="brz-columns css-1b0mf8p">
                                                                                    <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content">
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-image css-cfty7q">
                                                                                                                <picture>
                                                                                                                    <source srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=135&iH=135&oX=0&oY=0&cW=135&cH=135/f2c8eb9f4e4e572f8fb27d81e4965ca95590bb9b.jpeg 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=270&iH=270&oX=0&oY=0&cW=270&cH=270/f2c8eb9f4e4e572f8fb27d81e4965ca95590bb9b.jpeg 2x"
                                                                                                                            media="(min-width: 992px)">
                                                                                                                    <img class="brz-img"
                                                                                                                         src="/wp-content/uploads/brizy/pages/1.0.41/2/iW=400&iH=400&oX=0&oY=0&cW=400&cH=400/f2c8eb9f4e4e572f8fb27d81e4965ca95590bb9b.jpeg"
                                                                                                                         srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=400&iH=400&oX=0&oY=0&cW=400&cH=400/f2c8eb9f4e4e572f8fb27d81e4965ca95590bb9b.jpeg 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=800&iH=800&oX=0&oY=0&cW=800&cH=800/f2c8eb9f4e4e572f8fb27d81e4965ca95590bb9b.jpeg 2x">
                                                                                                                </picture>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="brz-columns css-19rgh32">
                                                                                    <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                                                        <div class="brz-bg-media">
                                                                                            <div class="brz-bg-color"></div>
                                                                                        </div>
                                                                                        <div class="brz-bg-content">
                                                                                            <div class="brz-wrapper css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex css-1ob677i">
                                                                                                            <div class="brz-rich-text">
                                                                                                                <p class="brz-tp-ilxdvbvksk">
                                                                                                                    <span style="color: rgb(122, 110, 102);">Venez nous rencontrer !</span>
                                                                                                                </p>
                                                                                                                <p class="brz-tp-ilxdvbvksk">
                                                                                                                    <span style="color: rgb(122, 110, 102);">Obtenez un rendez-vous&#xA0;</span>
                                                                                                                </p>
                                                                                                                <p class="brz-tp-ilxdvbvksk">
                                                                                                                    <span style="color: rgb(122, 110, 102);">d&#xE8;s aujourd&#x2019;hui !</span>
                                                                                                                </p></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="brz-wrapper-clone css-6fvry1">
                                                                                                <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-enr3lr">
                                                                                                    <div class="brz-bg-media">
                                                                                                        <div class="brz-bg-color"></div>
                                                                                                    </div>
                                                                                                    <div class="brz-bg-content">
                                                                                                        <div class="brz-d-xs-flex brz-flex-xs-wrap css-1aa05kl">
                                                                                                            <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                                                <a class="brz-a brz-btn css-1s0s6go"
                                                                                                                   href=""><span
                                                                                                                        class="brz-span brz-text__editor"
                                                                                                                        contenteditable="false">PRENDRE RENDEZ-VOUS</span></a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-9ezyjc">
                                                        <div class="brz-bg brz-d-xs-flex css-ngjkti">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "leifbg",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);