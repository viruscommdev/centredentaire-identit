<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("1-4photoText",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>

        <section id="qawqejrxth" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-nfy532">
                        <div class="brz-bg-media">
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-1rh7mn3">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-1cad2o0">
                                                    <div class="brz-columns css-t360gx">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-image css-cfty7q">
                                                                                    <picture>
                                                                                        <source srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=451&iH=449&oX=26&oY=0&cW=399&cH=449/ffb8ce4eb70d95fc8025912d9f4844ab6c923336.jpeg 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=902&iH=898&oX=52&oY=0&cW=798&cH=898/ffb8ce4eb70d95fc8025912d9f4844ab6c923336.jpeg 2x"
                                                                                                media="(min-width: 992px)">
                                                                                        <img class="brz-img"
                                                                                             src="/wp-content/uploads/brizy/pages/1.0.41/2/iW=400&iH=398&oX=0&oY=0&cW=400&cH=398/ffb8ce4eb70d95fc8025912d9f4844ab6c923336.jpeg"
                                                                                             srcset="/wp-content/uploads/brizy/pages/1.0.41/2/iW=400&iH=398&oX=0&oY=0&cW=400&cH=398/ffb8ce4eb70d95fc8025912d9f4844ab6c923336.jpeg 1x, /wp-content/uploads/brizy/pages/1.0.41/2/iW=800&iH=796&oX=0&oY=0&cW=800&cH=796/ffb8ce4eb70d95fc8025912d9f4844ab6c923336.jpeg 2x">
                                                                                    </picture>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-1wo1sl0">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-tp-ilxdvbvksk">
                                                                                        Offrez-vous un sourire &#xE0; votre
                                                                                        mesure&#xA0;</p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-400 brz-fw-im-400">
                                                                                        <span style="color: rgb(0, 0, 0);">En plus des services en dentisterie g&#xE9;n&#xE9;rale et pr&#xE9;ventive, nous vous offrons une multitude de soins esth&#xE9;tiques et chirurgicaux visant &#xE0; maximiser les qualit&#xE9;s fonctionnelles et l&#x2019;aspect visuel de votre dentition. Nos dentistes g&#xE9;n&#xE9;ralistes poss&#xE8;dent une expertise en endodontie, en parodontie, en implantologie de m&#xEA;me qu&#x2019;en orthodontie afin de vous proposer, sous un m&#xEA;me toit, tous les soins dont vous avez besoin.&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-400 brz-fw-im-400">
                                                                                        <span style="color: rgb(0, 0, 0);">&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-400 brz-fw-im-400">
                                                                                        <span style="color: rgb(0, 0, 0);">Que vous souffriez de maladies des dents ou des gencives ou que vous&#xA0;soyez insatisfaits de votre sourire &#xE0; cause de dents abim&#xE9;s, manquantes ou mal align&#xE9;es, nous avons une solution pour vous. Notre &#xE9;quipe d&#xE9;vou&#xE9;e saura vous proposer des traitements appropri&#xE9;s &#xE0; vos besoins et vos moyens,</span>
                                                                                    </p>
                                                                                    <p class="brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-400 brz-fw-im-400">
                                                                                        <span style="color: rgb(0, 0, 0);">&#xA0;afin que vous votre dentition ait les qualit&#xE9;s esth&#xE9;tiques et fonctionnelles&#xA0;que vous m&#xE9;ritez !&#xA0;</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper-clone css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-i9tzew">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex brz-flex-xs-wrap css-1aa05kl">
                                                                                <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                    <a class="brz-a brz-btn css-e2llf"
                                                                                       href=""><span
                                                                                            class="brz-span brz-text__editor"
                                                                                            contenteditable="false">CONTACTEZ-NOUS</span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "1-4photoText",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);