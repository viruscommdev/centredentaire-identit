<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("titleanddesc",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>

        <section id="jamoueuean" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-nfy532">
                        <div class="brz-bg-media">
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-uhsi1k">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-1cad2o0">
                                                    <div class="brz-columns css-19rf22h">
                                                        <div class="brz-bg brz-d-xs-flex css-qt2c83">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-tp-ilxdvbvksk brz-text-lg-center">
                                                                                        <span style="color: rgb(0, 0, 0);">Services dentaires&#xA0;</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-text-lg-center brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-400 brz-fw-im-400">
                                                                                        <span style="color: rgb(0, 0, 0);">Une offre multiple afin d&#x2019;assurer une prise en charge compl&#xE8;te qui ne laisse aucun&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-text-lg-center brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fs-lg-18 brz-fs-im-17 brz-lh-lg-1_4 brz-lh-im-1_3 brz-fw-lg-400 brz-fw-im-400">
                                                                                        <span style="color: rgb(0, 0, 0);">pan de votre sant&#xE9; bucco-dentaire de c&#xF4;t&#xE9;.&#xA0;</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "titleanddesc",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);