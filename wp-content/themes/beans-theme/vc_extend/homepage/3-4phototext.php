<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("3-4phototext",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>

        <section id="ustzwjzjtg" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-nfy532">
                        <div class="brz-bg-media">
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-1vys287">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-10jdpoz">
                                                    <div class="brz-columns css-ol0y3e">
                                                        <div class="brz-bg brz-d-xs-flex css-719m6a">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-video" data-mute="on" data-autoplay="on">
                                                                    <iframe class="brz-iframe brz-bg-video__cover"
                                                                            style="display:none"></iframe>
                                                                </div>
                                                                <div class="brz-bg-map">
                                                                    <iframe class="brz-iframe brz-bg-map__cover"
                                                                            style="display:none"></iframe>
                                                                </div>
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(0, 0, 0);">Traitement&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(0, 0, 0);">de cas avanc&#xE9;s&#xA0;</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1szh5xq">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-fw-im-400 brz-fw-lg-400 brz-lh-im-1_9 brz-lh-lg-1_4 brz-fs-im-6 brz-fs-lg-18 brz-ff-lato brz-ls-im-0 brz-ls-lg-0">
                                                                                        <span style="color: rgb(0, 0, 0);">Nous offrons des traitements sp&#xE9;cialis&#xE9;s&#xE0; la fine pointe de la technologie afin de prendre en charge une panoplie de cas,&#xA0;du plus simple au plus complexe : implantologie, greffe osseuse et&#xA0;&#xE9;l&#xE9;vation sinusale, traitement de canal&#xA0;et apectomie, greffes de gencives assist&#xE9;es au laser, etc.&#xA0;</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper-clone css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-i9tzew">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex brz-flex-xs-wrap css-1aa05kl">
                                                                                <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                    <a class="brz-a brz-btn css-e2llf"
                                                                                       href=""><span
                                                                                            class="brz-span brz-text__editor"
                                                                                            contenteditable="false">CONTACTEZ-NOUS</span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-1wa1wdi">
                                                        <div class="brz-bg brz-d-xs-flex css-r1x8zl">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-image"></div>
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "3-4phototext",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);