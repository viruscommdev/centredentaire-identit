<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/9/2018
 * Time: 3:23 PM
 */
$vc_module->all_in_one("1-2phototext",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(), $atts);
        ob_start();
        ?>

        <section id="rxqyquyges" class="brz-section css-lfbrd7">
            <div class="brz-section__items">
                <div class="brz-section__content">
                    <div class="brz-bg css-nfy532">
                        <div class="brz-bg-media">
                            <div class="brz-bg-color"></div>
                        </div>
                        <div class="brz-bg-content">
                            <div class="brz-container__wrap css-c7obq1">
                                <div class="brz-container css-fo2o23">
                                    <div class="brz-row__container">
                                        <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-e37x8q">
                                            <div class="brz-bg-media">
                                                <div class="brz-bg-color"></div>
                                            </div>
                                            <div class="brz-bg-content">
                                                <div class="brz-row css-10jdpoz">
                                                    <div class="brz-columns css-nsvst2">
                                                        <div class="brz-bg brz-d-xs-flex css-1670w4k">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-image"></div>
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1vp2wmn">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-tp-ilxdvbvksk brz-mt-lg-185">
                                                                                        <span style="color: rgb(255, 255, 255);">Bienvenue</span>
                                                                                    </p>
                                                                                    <p class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(255, 255, 255);">&#xA0;aux nouveaux&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(255, 255, 255);">patients</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1l144ec">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-lh-lg-1_1 brz-lh-im-1_6 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-fs-lg-25 brz-fs-im-7">
                                                                                        <em style="color: rgb(255, 255, 255);">Des
                                                                                            soins optimaux&#xA0;</em></p>
                                                                                    <p class="brz-lh-lg-1_1 brz-lh-im-1_6 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-fs-lg-25 brz-fs-im-7">
                                                                                        <em style="color: rgb(255, 255, 255);">par
                                                                                            une &#xE9;quipe de&#xA0;</em>
                                                                                    </p>
                                                                                    <p class="brz-lh-lg-1_1 brz-lh-im-1_6 brz-ls-lg-0 brz-ls-im-0 brz-ff-lato brz-fw-lg-300 brz-fw-im-300 brz-fs-lg-25 brz-fs-im-7">
                                                                                        <em style="color: rgb(255, 255, 255);">professionnels
                                                                                            d&#xE9;vou&#xE9;s !</em></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper-clone css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-enr3lr">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex brz-flex-xs-wrap css-1aa05kl">
                                                                                <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                    <a class="brz-a brz-btn css-1s0s6go"
                                                                                       href=""><span
                                                                                            class="brz-span brz-text__editor"
                                                                                            contenteditable="false">PRENDRE RENDEZ-VOUS</span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brz-columns css-nsvst2">
                                                        <div class="brz-bg brz-d-xs-flex css-vsuxk9">
                                                            <div class="brz-bg-media">
                                                                <div class="brz-bg-image"></div>
                                                                <div class="brz-bg-color"></div>
                                                            </div>
                                                            <div class="brz-bg-content">
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1vp2wmn">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(255, 255, 255);">Une clinique&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(255, 255, 255);">&#xE0; la fine pointe&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-tp-ilxdvbvksk"><span
                                                                                            style="color: rgb(255, 255, 255);">de la technologie&#xA0;</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1l144ec">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex css-1ob677i">
                                                                                <div class="brz-rich-text"><p
                                                                                        class="brz-fs-im-7 brz-fs-lg-25 brz-fw-im-300 brz-fw-lg-300 brz-ff-lato brz-ls-im-0 brz-ls-lg-0 brz-lh-im-1_6 brz-lh-lg-1_1">
                                                                                        <span style="color: rgb(255, 255, 255);">La technologie au service&#xA0;</span>
                                                                                    </p>
                                                                                    <p class="brz-fs-im-7 brz-fs-lg-25 brz-fw-im-300 brz-fw-lg-300 brz-ff-lato brz-ls-im-0 brz-ls-lg-0 brz-lh-im-1_6 brz-lh-lg-1_1">
                                                                                        <span style="color: rgb(255, 255, 255);">de votre confort.</span>
                                                                                    </p></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="brz-wrapper-clone css-6fvry1">
                                                                    <div class="brz-bg brz-d-xs-flex brz-flex-xs-wrap css-1s0uvq2">
                                                                        <div class="brz-bg-media">
                                                                            <div class="brz-bg-color"></div>
                                                                        </div>
                                                                        <div class="brz-bg-content">
                                                                            <div class="brz-d-xs-flex brz-flex-xs-wrap css-1aa05kl">
                                                                                <div class="brz-wrapper-clone__item css-16a2s3d">
                                                                                    <a class="brz-a brz-btn css-e2llf"
                                                                                       href=""><span
                                                                                            class="brz-span brz-text__editor"
                                                                                            contenteditable="false"><!--{{textfield:xyz123:nicknamewithnumber]]-->TECHNOLOGIE<!--{{endfield}}--></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    },
    "1-2phototext",[
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => "title",
            "param_name" => "title",
            "value" => "title"
        )
    ]);