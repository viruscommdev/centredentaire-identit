<?php
$vc_module->all_in_one("blog_vc_footer_en",
    function ($atts, $content) {
        $atts = shortcode_atts([
            "category" => "0",
            "tag" => "0"
        ], $atts);

        query_posts([
            'post_type' => "post",
            "post_status" => "publish",
            'posts_per_page' => 3,
            'suppress_filters' => true
        ]);
        $posts=[];
        while (have_posts()) {
            the_post();
            $posts[] = [
                "name" => get_the_title(),
                "link" => get_permalink(),
                "except" => get_the_excerpt(),
                "img" => wp_get_attachment_image_url(get_post_thumbnail_id(), "full"),
            ];
        }
//        var_dump($posts);
//        die("here");
        ob_start();
        ?>
        <style>
            .vc_custom_1533030036928 {
                padding-bottom: 40px !important;
            }

            .vc_custom_1532966143590 {
                padding-bottom: 40px !important;
            }

            .vc_custom_1533031356574 {
                background-color: #ffb71b !important;
            }
            /*third image*/
            .vc_custom_1533029677302 {
                background-image: url(<?=$posts[2]['img']?>) !important;
                background-position: center !important;
                background-repeat: no-repeat !important;
                background-size: cover !important;
            }
            /*second image*/
            .vc_custom_1533029680766 {
                background-image: url(<?=$posts[1]['img']?>) !important;
                background-position: center !important;
                background-repeat: no-repeat !important;
                background-size: cover !important;
            }

            .vc_custom_1532965977884 {
                background-color: #7a6e66 !important;
            }

            .vc_custom_1532965844142 {
                background-color: #7a6e66 !important;
            }

            .vc_custom_1532965971383 {
                padding-bottom: 15px !important;
            }
            /*first image*/
            .vc_custom_1533029671429 {
                background-image: url( <?=$posts[0]['img']?>) !important;
                background-position: center !important;
                background-repeat: no-repeat !important;
                background-size: cover !important;
            }
            .blog_vc_footer a:hover {
                text-decoration: none!important;
            }
            .blog_vc_footer a:visited {
                 color: inherit!important;
             }

        </style>
        <div class="vc_row wpb_row vc_row-fluid padding138x padding60yt containerwp vc_row-o-equal-height vc_row-flex blog_vc_footer">
            <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-flex">
                            <div class="mobileCenter wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  vc_custom_1532965971383">
                                            <div class="wpb_wrapper">
                                                <p class="Lato font36 light">Blog</p>
                                                <p class="Lato light font18" style="padding-right: 10px;">Maintain good oral health with these tips and information from our team of dedicated professionals.</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="minHImg wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill" href="<?=$posts[0]['link']?>">
                                <div class="vc_column-inner vc_custom_1533029671429">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <a class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill" href="<?=$posts[0]['link']?>">
                <div class="vc_column-inner vc_custom_1532965844142">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1532965977884 vc_row-has-fill vc_row-o-equal-height vc_row-flex">
                            <div class="mobileCenter wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  colorWhite padding pleft20 ptop30">
                                            <div class="wpb_wrapper">
                                                <p class="font22 light Lato">
                                                    <?=$posts[0]['name']?>
                                                </p>
                                                <p class="font16 light Lato">
                                                    <?=$posts[0]['except']?></p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="vc_row wpb_row vc_row-fluid padding138x containerwp padding60yb vc_row-o-equal-height vc_row-flex blog_vc_footer">
            <a class="wpb_column vc_column_container vc_col-sm-6" href="<?=$posts[1]['link']?>">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-flex">
                            <div class="minHImg wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
                                <div class="vc_column-inner vc_custom_1533029680766">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                            <div class="mobileCenter wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
                                <div class="vc_column-inner vc_custom_1533031356574">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  vc_custom_1532966143590 colorWhite padding pleft20 ptop30">
                                            <div class="wpb_wrapper">
                                                <p class="font22 light Lato"><?=$posts[1]['name']?></p>
                                                <p class="font16 light Lato"><?=$posts[1]['except']?></p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            <a class="wpb_column vc_column_container vc_col-sm-6" href="<?=$posts[2]['link']?>">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-flex">
                            <div class="minHImg wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
                                <div class="vc_column-inner vc_custom_1533029677302">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                            <div class="mobileCenter wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  vc_custom_1533030036928 padding pleft20 ptop30 noPaddingMobile">
                                            <div class="wpb_wrapper">
                                                <p class="font22 light Lato"><?=$posts[2]['name']?></p>
                                                <p class="font16 light Lato"><?=$posts[2]['except']?></p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <?php
        return ob_get_clean();
    },
    "Blog vc footer EN",
    [

    ]);