<?php
$vc_module->all_in_one("Blog_sidebar",
    function ($atts, $content) {
        $atts = shortcode_atts([
                "category"=>"0",
                "tag"=>"0"
        ], $atts);
        $args=[
            'post_type'=>"post",
            "post_status"=>"publish",
            'posts_per_page' => 3
        ];
             
        query_posts( $args);
        ob_start();
        ?>
        <div class="blogPageSidebar">
        <h2 class="font18 bold lato" style="color:#7A6E66">ARTICLES LES PLUS RÉCENTS</h2>
            <div class="list">

                <?php

                while (have_posts()) {
                  
                    the_post();
                    ?>
                    <div class="articleSidebar">
                    
                             <div class="vc_col-sm-4">
                       <a href="<?=get_permalink()?>" class="default">  <div class="img">
                            <?= wp_get_attachment_image(get_post_thumbnail_id( ), "thumbnail") ?>
                        </div> </a>
                        </div>
                        <div class="vc_col-sm-8">
                        <div class="entry-meta">
                             <div class="titleArticle">
                              <h3 class="font16">  <?=get_the_title()?> </h3>
                              <p><span  class='author font14 lato regular'>Publié par: IdentiT <br /> le <?=get_the_date('d M Y')?>  </span></p>
                            </div>
                             </div>
                        </div>
                        
                        
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    },
    "Blog list sidebar",
    [

    ]);