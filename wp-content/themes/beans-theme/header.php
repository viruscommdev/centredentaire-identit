<?php
/*
Template Name: Snarfer
*/
?>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <?php wp_head(); ?>
  
<style>
.brz .brz-ff-abeezee {
        font-family: ABeeZee, sans-serif !important;
    }

    .brz .brz-ff-lato {
        font-family: Lato, sans-serif !important;
    }

    .brz .brz-ff-montserrat {
        font-family: Montserrat, sans-serif !important;
    }

    .brz .brz-ff-noto_serif {
        font-family: Noto Serif, serif !important;
    }

    .brz .brz-ff-playfair_display {
        font-family: Playfair Display, serif !important;
    }

    .brz .brz-ff-roboto {
        font-family: Roboto, sans-serif !important;
    }</style>
<style>
.brz .brz-cp-color1 {
        color: #191b21;
    }

    .brz .brz-cp-color2 {
        color: #142850;
    }

    .brz .brz-cp-color3 {
        color: #239ddb;
    }

    .brz .brz-cp-color4 {
        color: #66738d;
    }

    .brz .brz-cp-color5 {
        color: #bde1f4;
    }

    .brz .brz-cp-color6 {
        color: #eef0f2;
    }

    .brz .brz-cp-color7 {
        color: #73777f;
    }

    .brz .brz-cp-color8 {
        color: #ffffff;
    }</style>
<style>
.brz .brz-tp-paragraph {
        font-family: Noto Serif, serif;
        font-size: 16px;
        font-weight: 300;
        letter-spacing: 0px;
        line-height: 1.7em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-paragraph {
            font-size: 15px;
            font-weight: 300;
            letter-spacing: 0px;
            line-height: 1.6em;
        }
    }

    .brz .brz-tp-subtitle {
        font-family: Noto Serif, serif;
        font-size: 18px;
        font-weight: 300;
        letter-spacing: 0px;
        line-height: 1.5em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-subtitle {
            font-size: 17px;
            font-weight: 300;
            letter-spacing: 0px;
            line-height: 1.5em;
        }
    }

    .brz .brz-tp-abovetitle {
        font-family: Montserrat, sans-serif;
        font-size: 16px;
        font-weight: 400;
        letter-spacing: 2px;
        line-height: 1.7em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-abovetitle {
            font-size: 13px;
            font-weight: 400;
            letter-spacing: 2px;
            line-height: 1.7em;
        }
    }

    .brz .brz-tp-heading1 {
        font-family: Montserrat, sans-serif;
        font-size: 56px;
        font-weight: 200;
        letter-spacing: -1.5px;
        line-height: 1.3em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-heading1 {
            font-size: 34px;
            font-weight: 200;
            letter-spacing: -1px;
            line-height: 1.3em;
        }
    }

    .brz .brz-tp-heading2 {
        font-family: Montserrat, sans-serif;
        font-size: 42px;
        font-weight: 700;
        letter-spacing: -1.5px;
        line-height: 1.3em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-heading2 {
            font-size: 29px;
            font-weight: 700;
            letter-spacing: -0.5px;
            line-height: 1.3em;
        }
    }

    .brz .brz-tp-heading3 {
        font-family: Montserrat, sans-serif;
        font-size: 32px;
        font-weight: 600;
        letter-spacing: -1px;
        line-height: 1.3em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-heading3 {
            font-size: 22px;
            font-weight: 600;
            letter-spacing: 0px;
            line-height: 1.3em;
        }
    }

    .brz .brz-tp-heading4 {
        font-family: Montserrat, sans-serif;
        font-size: 26px;
        font-weight: 500;
        letter-spacing: -1px;
        line-height: 1.4em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-heading4 {
            font-size: 21px;
            font-weight: 500;
            letter-spacing: 0px;
            line-height: 1.4em;
        }
    }

    .brz .brz-tp-heading5 {
        font-family: Montserrat, sans-serif;
        font-size: 20px;
        font-weight: 500;
        letter-spacing: 0px;
        line-height: 1.5em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-heading5 {
            font-size: 18px;
            font-weight: 500;
            letter-spacing: 0px;
            line-height: 1.4em;
        }
    }

    .brz .brz-tp-heading6 {
        font-family: Montserrat, sans-serif;
        font-size: 17px;
        font-weight: 500;
        letter-spacing: 0px;
        line-height: 1.5em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-heading6 {
            font-size: 16px;
            font-weight: 500;
            letter-spacing: 0px;
            line-height: 1.4em;
        }
    }

    .brz .brz-tp-button {
        font-family: Montserrat, sans-serif;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: 3px;
        line-height: 1.8em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-button {
            font-size: 12px;
            font-weight: 600;
            letter-spacing: 3px;
            line-height: 1.8em;
        }
    }

    .brz .brz-tp-ilxdvbvksk {
        font-family: Lato, sans-serif;
        font-size: 36px;
        font-weight: 300;
        letter-spacing: 0px;
        line-height: 1.1em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-ilxdvbvksk {
            font-size: 15px;
            font-weight: 300;
            letter-spacing: 0px;
            line-height: 1.6em;
        }
    }

    .brz .brz-tp-fcbntaxquh {
        font-family: Noto Serif, serif;
        font-size: 16px;
        font-weight: 300;
        letter-spacing: 0px;
        line-height: 1.7em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-fcbntaxquh {
            font-size: 15px;
            font-weight: 300;
            letter-spacing: 0px;
            line-height: 1.6em;
        }
    }

    .brz .brz-tp-nhdtypzlzo {
        font-family: Lato, sans-serif;
        font-size: 14px;
        font-weight: 400;
        letter-spacing: 0px;
        line-height: 1.4em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-nhdtypzlzo {
            font-size: 15px;
            font-weight: 300;
            letter-spacing: 0px;
            line-height: 1.6em;
        }
    }

    .brz .brz-tp-ctigfdfdgy {
        font-family: Lato, sans-serif;
        font-size: 26px;
        font-weight: 300;
        letter-spacing: 0px;
        line-height: 1.1em;
    }

    @media (max-width: 767px) {
        .brz .brz-tp-ctigfdfdgy {
            font-size: 15px;
            font-weight: 300;
            letter-spacing: 0px;
            line-height: 1.6em;
        }
    }
</style>
<style>
.css-10qcoa1, [data-css-10qcoa1] {
        z-index: auto;
    }

    .brz .css-10qcoa1, .brz [data-css-10qcoa1] {
        display: block;
    }

    .brz .css-10qcoa1 .brz-section__content, .brz [data-css-10qcoa1] .brz-section__content {
        height: 100%;
    }

    .brz .css-10qcoa1 .brz-slick-slider__dots, .brz [data-css-10qcoa1] .brz-slick-slider__dots {
        color: rgba(25, 27, 33, 1);
    }

    .brz .css-10qcoa1 .brz-slick-slider__arrow, .brz [data-css-10qcoa1] .brz-slick-slider__arrow {
        color: rgba(255, 255, 255, 1);
    }

    @media (max-width: 767px) {
        .brz .css-10qcoa1, .brz [data-css-10qcoa1] {
            display: block;
        }
    }

    .css-1xwvmrg > .brz-bg-media, [data-css-1xwvmrg] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-1xwvmrg > .brz-bg-media > .brz-bg-image, [data-css-1xwvmrg] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/e84b7691245c6041db0089f919cda161be74af9c.png);
        background-position: 50% 50%;
        background-attachment: scroll;
    }

    .css-1xwvmrg > .brz-bg-media > .brz-bg-color, [data-css-1xwvmrg] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(255, 255, 255, 0);
    }

    .css-1xwvmrg > .brz-bg-media > .brz-bg-map, [data-css-1xwvmrg] > .brz-bg-media > .brz-bg-map {
        display: none;
    }

    @media (max-width: 767px) {
        .css-1xwvmrg > .brz-bg-media > .brz-bg-image, [data-css-1xwvmrg] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/e84b7691245c6041db0089f919cda161be74af9c.png);
            background-position: 50% 50%;
            background-attachment: scroll;
        }

        .css-1xwvmrg > .brz-bg-media > .brz-bg-color, [data-css-1xwvmrg] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(255, 255, 255, 0);
        }

        .css-1xwvmrg > .brz-bg-media > .brz-bg-map, [data-css-1xwvmrg] > .brz-bg-media > .brz-bg-map {
            display: none;
        }
    }

    .css-fo2o23, [data-css-fo2o23] {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
    }

    @media (min-width: 768px) {
        .css-fo2o23, [data-css-fo2o23] {
            max-width: 100%;
        }
    }

    .css-b8uy3h, [data-css-b8uy3h] {
        
        padding-top: 271px;
        padding-bottom: 250px;
    }

    @media (max-width: 767px) {
        .css-b8uy3h, [data-css-b8uy3h] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    .brz .css-6fvry1, .brz [data-css-6fvry1] {
        display: block;
    }

    @media (max-width: 767px) {
        .brz .css-6fvry1, .brz [data-css-6fvry1] {
            display: block;
        }
    }

    .css-47lv8j, [data-css-47lv8j] {
        justify-content: center;
        padding-top: 0px;
        padding-right: 150px;
        padding-bottom: 0px;
        padding-left: 0px;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
    }

    @media (max-width: 767px) {
        .css-47lv8j, [data-css-47lv8j] {
            justify-content: center;
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
        }
    }

    .css-55m1q3, [data-css-55m1q3] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 10px;
        margin-left: 0px;
    }

    @media (max-width: 767px) {
        .css-55m1q3, [data-css-55m1q3] {
            margin-top: 10px;
            margin-right: 0px;
            margin-bottom: 10px;
            margin-left: 0px;
        }
    }

    .css-1ob677i, [data-css-1ob677i] {
        justify-content: center;
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
    }

    @media (max-width: 767px) {
        .css-1ob677i, [data-css-1ob677i] {
            justify-content: center;
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
        }
    }

    .css-1trd8bt, [data-css-1trd8bt] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    @media (max-width: 767px) {
        .css-1trd8bt, [data-css-1trd8bt] {
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
            margin-left: 0px;
        }
    }

    .brz .css-s45j78, .brz [data-css-s45j78] {
        height: 10px;
    }

    @media (max-width: 767px) {
        .brz .css-s45j78, .brz [data-css-s45j78] {
            height: 10px;
        }
    }

    .brz .css-19zyqwf, .brz [data-css-19zyqwf] {
        height: 30px;
    }

    @media (max-width: 767px) {
        .brz .css-19zyqwf, .brz [data-css-19zyqwf] {
            height: 30px;
        }
    }

    .css-1xbwuqn > .brz-bg-media, [data-css-1xbwuqn] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-1xbwuqn > .brz-bg-media > .brz-bg-image, [data-css-1xbwuqn] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/ad09ef4d07c08374f0e13ede6f0d3c05cd9bba47.jpeg);
        background-position: 50% 50%;
        background-attachment: scroll;
    }

    .css-1xbwuqn > .brz-bg-media > .brz-bg-color, [data-css-1xbwuqn] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(255, 255, 255, 0.9);
    }

    .css-1xbwuqn > .brz-bg-media > .brz-bg-map, [data-css-1xbwuqn] > .brz-bg-media > .brz-bg-map {
        display: none;
    }

    @media (max-width: 767px) {
        .css-1xbwuqn > .brz-bg-media > .brz-bg-image, [data-css-1xbwuqn] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/ad09ef4d07c08374f0e13ede6f0d3c05cd9bba47.jpeg);
            background-position: 50% 50%;
            background-attachment: scroll;
        }

        .css-1xbwuqn > .brz-bg-media > .brz-bg-color, [data-css-1xbwuqn] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(255, 255, 255, 0.9);
        }

        .css-1xbwuqn > .brz-bg-media > .brz-bg-map, [data-css-1xbwuqn] > .brz-bg-media > .brz-bg-map {
            display: none;
        }
    }

    .css-1nars7o, [data-css-1nars7o] {
        
        padding-top: 250px;
        padding-bottom: 250px;
    }

    @media (max-width: 767px) {
        .css-1nars7o, [data-css-1nars7o] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    .css-18qc318 > .brz-bg-media, [data-css-18qc318] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-18qc318 > .brz-bg-media > .brz-bg-image, [data-css-18qc318] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/3715ae7da78bcde1fe3d0fe9f129cfd86edb0d76.jpeg);
        background-position: 50% 50%;
        background-attachment: scroll;
    }

    .css-18qc318 > .brz-bg-media > .brz-bg-color, [data-css-18qc318] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(255, 255, 255, 0.9);
    }

    .css-18qc318 > .brz-bg-media > .brz-bg-map, [data-css-18qc318] > .brz-bg-media > .brz-bg-map {
        display: none;
    }

    @media (max-width: 767px) {
        .css-18qc318 > .brz-bg-media > .brz-bg-image, [data-css-18qc318] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/3715ae7da78bcde1fe3d0fe9f129cfd86edb0d76.jpeg);
            background-position: 50% 50%;
            background-attachment: scroll;
        }

        .css-18qc318 > .brz-bg-media > .brz-bg-color, [data-css-18qc318] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(255, 255, 255, 0.9);
        }

        .css-18qc318 > .brz-bg-media > .brz-bg-map, [data-css-18qc318] > .brz-bg-media > .brz-bg-map {
            display: none;
        }
    }

    .css-1szh5xq, [data-css-1szh5xq] {
        z-index: auto;
        margin-right: 0px;
        margin-left: 0px;
    }

    @media (max-width: 767px) {
        .css-1szh5xq, [data-css-1szh5xq] {
            margin-top: 10px;
            margin-right: 0px;
            margin-bottom: 10px;
            margin-left: 0px;
        }
    }

    .css-2v55jk, [data-css-2v55jk] {
        justify-content: center;
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
        flex: 1 1 auto;
        margin-top: 0px;
        margin-right: -5px;
        margin-bottom: -20px;
        margin-left: -5px;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -webkit-flex: 1 1 auto;
    }

    @media (max-width: 767px) {
        .css-2v55jk, [data-css-2v55jk] {
            justify-content: center;
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
        }
    }

    .css-16a2s3d, [data-css-16a2s3d] {
        padding-top: 0px;
        padding-right: 5px;
        padding-bottom: 20px;
        padding-left: 5px;
    }

    .brz .css-39pc8i, .brz [data-css-39pc8i] {
        flex-flow: row-reverse nowrap;
        font-family: Montserrat, sans-serif;
        font-size: 12px;
        line-height: 1.8;
        font-weight: 600;
        letter-spacing: 3px;
        color: rgba(255, 255, 255, 1);
        border-color: rgba(35, 157, 219, 1);
        background-color: rgba(35, 157, 219, 1);
        border-width: 2px;
        border-style: solid;
        padding-top: 14px;
        padding-right: 42px;
        padding-bottom: 14px;
        padding-left: 42px;
        border-radius: 4px;
        -webkit-flex-flow: row-reverse nowrap;
    }

    .brz .css-39pc8i.css-39pc8i:hover, .brz [data-css-39pc8i][data-css-39pc8i]:hover {
        color: rgba(255, 255, 255, 1);
        border-color: rgba(20, 40, 80, 0);
        background-color: rgba(20, 40, 80, 1);
    }

    @media (max-width: 767px) {
        .brz .css-39pc8i, .brz [data-css-39pc8i] {
            font-size: 12px;
            font-weight: 600;
            line-height: 1.8;
            letter-spacing: 3px;
            padding-top: 11px;
            padding-right: 26px;
            padding-bottom: 11px;
            padding-left: 26px;
            border-radius: 4px;
        }
    }

    .css-mc5bsv > .brz-bg-media, [data-css-mc5bsv] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-mc5bsv > .brz-bg-media > .brz-bg-image, [data-css-mc5bsv] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/7c627a8c2eece4831ff43399b8edad32eab7e561.jpeg);
        background-position: 50% 50%;
        background-attachment: scroll;
    }

    .css-mc5bsv > .brz-bg-media > .brz-bg-color, [data-css-mc5bsv] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(255, 255, 255, 0.9);
    }

    .css-mc5bsv > .brz-bg-media > .brz-bg-map, [data-css-mc5bsv] > .brz-bg-media > .brz-bg-map {
        display: none;
    }

    @media (max-width: 767px) {
        .css-mc5bsv > .brz-bg-media > .brz-bg-image, [data-css-mc5bsv] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/7c627a8c2eece4831ff43399b8edad32eab7e561.jpeg);
            background-position: 50% 50%;
            background-attachment: scroll;
        }

        .css-mc5bsv > .brz-bg-media > .brz-bg-color, [data-css-mc5bsv] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(255, 255, 255, 0.9);
        }

        .css-mc5bsv > .brz-bg-media > .brz-bg-map, [data-css-mc5bsv] > .brz-bg-media > .brz-bg-map {
            display: none;
        }
    }

    .css-yf3bnc, [data-css-yf3bnc] {
        justify-content: flex-end;
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
        flex: 1 1 auto;
        margin-top: 0px;
        margin-right: -5px;
        margin-bottom: -20px;
        margin-left: -5px;
        -webkit-box-pack: end;
        -webkit-justify-content: flex-end;
        -webkit-flex: 1 1 auto;
    }

    @media (max-width: 767px) {
        .css-yf3bnc, [data-css-yf3bnc] {
            justify-content: flex-end;
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            -webkit-box-pack: end;
            -webkit-justify-content: flex-end;
        }
    }

    .css-lfbrd7, [data-css-lfbrd7] {
        z-index: auto;
    }

    .brz .css-lfbrd7, .brz [data-css-lfbrd7] {
        display: block;
    }

    .brz .css-lfbrd7 .brz-section__content, .brz [data-css-lfbrd7] .brz-section__content {
        min-height: 100%;
    }

    .brz .css-lfbrd7 .brz-slick-slider__dots, .brz [data-css-lfbrd7] .brz-slick-slider__dots {
        color: rgba(0, 0, 0, 1);
    }

    .brz .css-lfbrd7 .brz-slick-slider__arrow, .brz [data-css-lfbrd7] .brz-slick-slider__arrow {
        color: rgba(0, 0, 0, 1);
    }

    @media (max-width: 767px) {
        .brz .css-lfbrd7, .brz [data-css-lfbrd7] {
            display: block;
        }
    }

    .css-nfy532 > .brz-bg-media, [data-css-nfy532] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-nfy532 > .brz-bg-media > .brz-bg-image, [data-css-nfy532] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
        background-attachment: scroll;
    }

    .css-nfy532 > .brz-bg-media > .brz-bg-color, [data-css-nfy532] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    .css-nfy532 > .brz-bg-media > .brz-bg-map, [data-css-nfy532] > .brz-bg-media > .brz-bg-map {
        display: none;
    }

    @media (max-width: 767px) {
        .css-nfy532 > .brz-bg-media > .brz-bg-image, [data-css-nfy532] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
            background-attachment: scroll;
        }

        .css-nfy532 > .brz-bg-media > .brz-bg-color, [data-css-nfy532] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }


        .css-nfy532 > .brz-bg-media > .brz-bg-map, [data-css-nfy532] > .brz-bg-media > .brz-bg-map {
            display: none;
        }
    }

    .css-c7obq1, [data-css-c7obq1] {
        
        padding-top: 15px;
        padding-bottom: 15px;
    }

    @media (max-width: 767px) {
        .css-c7obq1, [data-css-c7obq1] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    .css-e37x8q, [data-css-e37x8q] {
        z-index: auto;
        margin-left: auto;
        margin-right: auto;
        margin-top: 0px;
        margin-bottom: 0px;
    }

    .css-e37x8q > .brz-bg-media, [data-css-e37x8q] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-e37x8q > .brz-bg-content, [data-css-e37x8q] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
    }

    .css-e37x8q > .brz-bg-media > .brz-bg-image, [data-css-e37x8q] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
    }

    .css-e37x8q > .brz-bg-media > .brz-bg-color, [data-css-e37x8q] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    .css-e37x8q > .brz-bg-media > .brz-bg-map, [data-css-e37x8q] > .brz-bg-media > .brz-bg-map {
        display: none;
    }

    @media (max-width: 767px) {
        .css-e37x8q, [data-css-e37x8q] {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .css-e37x8q > .brz-bg-media > .brz-bg-image, [data-css-e37x8q] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
        }

        .css-e37x8q > .brz-bg-media > .brz-bg-color, [data-css-e37x8q] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }

        .css-e37x8q > .brz-bg-media > .brz-bg-map, [data-css-e37x8q] > .brz-bg-media > .brz-bg-map {
            display: none;
        }
    }

    @media (min-width: 768px) {
        .brz .css-e37x8q, .brz [data-css-e37x8q] {
            align-items: flex-start;
            max-width: 100%;
            min-height: auto;
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
        }
    }

   
    @media (min-width: 768px) {
        .brz .css-1hba4o8, .brz [data-css-1hba4o8] {
            will-change: flex, max-width;
            flex: 1 1 20%;
            max-width: 20%;
            -webkit-flex: 1 1 20%;
        }
    }

    .css-qt2c83, [data-css-qt2c83] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    .css-qt2c83 > .brz-bg-content, [data-css-qt2c83] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-right: 15px;
        padding-bottom: 5px;
        padding-left: 15px;
    }

    .css-qt2c83 > .brz-bg-media, [data-css-qt2c83] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-qt2c83 > .brz-bg-media > .brz-bg-image, [data-css-qt2c83] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
    }

    .css-qt2c83 > .brz-bg-media > .brz-bg-color, [data-css-qt2c83] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (min-width: 768px) {
        .brz .css-qt2c83, .brz [data-css-qt2c83] {
            align-items: flex-start;
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
        }
    }

    @media (max-width: 767px) {
        .css-qt2c83, [data-css-qt2c83] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-qt2c83 > .brz-bg-content, [data-css-qt2c83] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
        }

        .css-qt2c83 > .brz-bg-media > .brz-bg-image, [data-css-qt2c83] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
        }

        .css-qt2c83 > .brz-bg-media > .brz-bg-color, [data-css-qt2c83] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-bb3pb6, [data-css-bb3pb6] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    .css-bb3pb6 > .brz-bg-content, [data-css-bb3pb6] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 35px;
        padding-right: 15px;
        padding-bottom: 5px;
        padding-left: 15px;
    }

    .css-bb3pb6 > .brz-bg-media, [data-css-bb3pb6] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-bb3pb6 > .brz-bg-media > .brz-bg-image, [data-css-bb3pb6] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
    }

    .css-bb3pb6 > .brz-bg-media > .brz-bg-color, [data-css-bb3pb6] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (min-width: 768px) {
        .brz .css-bb3pb6, .brz [data-css-bb3pb6] {
            align-items: flex-start;
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
        }
    }

    @media (max-width: 767px) {
        .css-bb3pb6, [data-css-bb3pb6] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-bb3pb6 > .brz-bg-content, [data-css-bb3pb6] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
        }

        .css-bb3pb6 > .brz-bg-media > .brz-bg-image, [data-css-bb3pb6] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
        }

        .css-bb3pb6 > .brz-bg-media > .brz-bg-color, [data-css-bb3pb6] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .brz .css-15x3uj2, .brz [data-css-15x3uj2] {
        width: 100%;
    }

    .brz .css-15x3uj2 .brz-hr, .brz [data-css-15x3uj2] .brz-hr {
        border-top-width: 2px;
        border-top-style: solid;

        border-top-color: rgba(115, 119, 127, 0.75);
    }

    @media (max-width: 767px) {
        .brz .css-15x3uj2, .brz [data-css-15x3uj2] {
            width: 100%;
        }

        .brz .css-15x3uj2 .brz-hr, .brz [data-css-15x3uj2] .brz-hr {
            border-top-width: 2px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-7akqbx, .brz [data-css-7akqbx] {
            will-change: flex, max-width;
            flex: 1 1 18.7%;
            max-width: 18.7%;
            -webkit-flex: 1 1 18.7%;
        }
    }

    @media (min-width: 768px) {
        .brz .css-18v4mev, .brz [data-css-18v4mev] {
            will-change: flex, max-width;
            flex: 1 1 23.2%;
            max-width: 23.2%;
            -webkit-flex: 1 1 23.2%;
        }
    }

    .css-fmjz2y, [data-css-fmjz2y] {
        z-index: auto;
        margin-top: 35px;
        margin-right: 0px;
        margin-bottom: 10px;
        margin-left: 0px;
    }

    @media (max-width: 767px) {
        .css-fmjz2y, [data-css-fmjz2y] {
            margin-top: 10px;
            margin-right: 0px;
            margin-bottom: 10px;
            margin-left: 0px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-x3v29r, .brz [data-css-x3v29r] {
            will-change: flex, max-width;
            flex: 1 1 18.1%;
            max-width: 18.1%;
            -webkit-flex: 1 1 18.1%;
        }
    }

    .css-10jdpoz, [data-css-10jdpoz] {
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
    }

    @media (max-width: 767px) {
        .css-10jdpoz, [data-css-10jdpoz] {
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-nsvst2, .brz [data-css-nsvst2] {
            will-change: flex, max-width;
            flex: 1 1 50%;
            max-width: 50%;
            -webkit-flex: 1 1 50%;
        }
    }

    .css-1670w4k, [data-css-1670w4k] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 8px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    .css-1670w4k > .brz-bg-content, [data-css-1670w4k] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 5px;
        padding-right: 15px;
        padding-bottom: 5px;
        padding-left: 46px;
    }

    .css-1670w4k > .brz-bg-media, [data-css-1670w4k] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-1670w4k > .brz-bg-media > .brz-bg-image, [data-css-1670w4k] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/255a0061fc76d92ba5264d7aa94b5cef0d8f7a0a.jpeg);
        background-position: 50% 50%;
    }

    .css-1670w4k > .brz-bg-media > .brz-bg-color, [data-css-1670w4k] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (min-width: 768px) {
        .brz .css-1670w4k, .brz [data-css-1670w4k] {
            align-items: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
        }
    }

    @media (max-width: 767px) {
        .css-1670w4k, [data-css-1670w4k] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-1670w4k > .brz-bg-content, [data-css-1670w4k] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
        }

        .css-1670w4k > .brz-bg-media > .brz-bg-image, [data-css-1670w4k] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/255a0061fc76d92ba5264d7aa94b5cef0d8f7a0a.jpeg);
            background-position: 50% 50%;
        }

        .css-1670w4k > .brz-bg-media > .brz-bg-color, [data-css-1670w4k] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-1vp2wmn, [data-css-1vp2wmn] {
        z-index: auto;
        margin-top: 185px;
        margin-right: 0px;
        margin-bottom: 10px;
        margin-left: 0px;
    }

    @media (max-width: 767px) {
        .css-1vp2wmn, [data-css-1vp2wmn] {
            margin-top: 10px;
            margin-right: 0px;
            margin-bottom: 10px;
            margin-left: 0px;
        }
    }

    .css-1l144ec, [data-css-1l144ec] {
        z-index: auto;
        margin-top: 4px;
        margin-right: 0px;
        margin-bottom: 10px;
        margin-left: 0px;
    }

    @media (max-width: 767px) {
        .css-1l144ec, [data-css-1l144ec] {
            margin-top: 10px;
            margin-right: 0px;
            margin-bottom: 10px;
            margin-left: 0px;
        }
    }

    .css-1aa05kl, [data-css-1aa05kl] {
        justify-content: flex-start;
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
        flex: 1 1 auto;
        margin-top: 0px;
        margin-right: -5px;
        margin-bottom: -20px;
        margin-left: -5px;
        -webkit-box-pack: start;
        -webkit-justify-content: flex-start;
        -webkit-flex: 1 1 auto;
    }

    @media (max-width: 767px) {
        .css-1aa05kl, [data-css-1aa05kl] {
            justify-content: flex-start;
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
        }
    }

    .css-enr3lr, [data-css-enr3lr] {
        z-index: auto;
        margin-top: 10px;
        margin-right: 0px;
        margin-bottom: 146px;
        margin-left: 0px;
    }

    @media (max-width: 767px) {
        .css-enr3lr, [data-css-enr3lr] {
            margin-top: 10px;
            margin-right: 0px;
            margin-bottom: 10px;
            margin-left: 0px;
        }
    }

    .brz .css-1s0s6go, .brz [data-css-1s0s6go] {
        flex-flow: row-reverse nowrap;
        font-family: Lato, sans-serif;
        font-size: 11px;
        line-height: 1.1;
        font-weight: 400;
        letter-spacing: 0;
        color: rgba(255, 255, 255, 1);
        border-color: rgba(35, 157, 219, 0);
        background-color: rgba(255, 183, 27, 1);
        border-width: 0;
        border-style: solid;
        padding-top: 11px;
        padding-right: 26px;
        padding-bottom: 11px;
        padding-left: 26px;
        border-radius: 2px;
        -webkit-flex-flow: row-reverse nowrap;
    }

    .brz .css-1s0s6go.css-1s0s6go:hover, .brz [data-css-1s0s6go][data-css-1s0s6go]:hover {
        color: rgba(255, 255, 255, 1);
        border-color: rgba(35, 157, 219, 0);
        background-color: rgba(255, 183, 27, 0.54);
    }

    @media (max-width: 767px) {
        .brz .css-1s0s6go, .brz [data-css-1s0s6go] {
            font-size: 15px;
            font-weight: 300;
            line-height: 1.6;
            letter-spacing: 0px;
            padding-top: 11px;
            padding-right: 26px;
            padding-bottom: 11px;
            padding-left: 26px;
            border-radius: 2px;
        }
    }

    .css-vsuxk9, [data-css-vsuxk9] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 8px;
    }

    .css-vsuxk9 > .brz-bg-content, [data-css-vsuxk9] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 5px;
        padding-right: 15px;
        padding-bottom: 5px;
        padding-left: 37px;
    }

    .css-vsuxk9 > .brz-bg-media, [data-css-vsuxk9] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-vsuxk9 > .brz-bg-media > .brz-bg-image, [data-css-vsuxk9] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/88baad0a3f1feeac603aeae5b0e8c52b1a7afc55.jpeg);
        background-position: 50% 50%;
    }

    .css-vsuxk9 > .brz-bg-media > .brz-bg-color, [data-css-vsuxk9] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (min-width: 768px) {
        .brz .css-vsuxk9, .brz [data-css-vsuxk9] {
            align-items: flex-start;
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
        }
    }

    @media (max-width: 767px) {
        .css-vsuxk9, [data-css-vsuxk9] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-vsuxk9 > .brz-bg-content, [data-css-vsuxk9] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 10px;
            padding-bottom: 0px;
            padding-left: 10px;
        }

        .css-vsuxk9 > .brz-bg-media > .brz-bg-image, [data-css-vsuxk9] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/88baad0a3f1feeac603aeae5b0e8c52b1a7afc55.jpeg);
            background-position: 50% 50%;
        }

        .css-vsuxk9 > .brz-bg-media > .brz-bg-color, [data-css-vsuxk9] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-1s0uvq2, [data-css-1s0uvq2] {
        z-index: auto;
        margin-top: 38px;
        margin-right: 0px;
        margin-bottom: 146px;
        margin-left: 0px;
    }

    @media (max-width: 767px) {
        .css-1s0uvq2, [data-css-1s0uvq2] {
            margin-top: 10px;
            margin-right: 0px;
            margin-bottom: 10px;
            margin-left: 0px;
        }
    }

    .brz .css-e2llf, .brz [data-css-e2llf] {
        flex-flow: row-reverse nowrap;
        font-family: Lato, sans-serif;
        font-size: 11px;
        line-height: 1.1;
        font-weight: 400;
        letter-spacing: 0;
        color: rgba(255, 255, 255, 1);
        border-color: rgba(35, 157, 219, 0);
        background-color: rgba(122, 110, 102, 1);
        border-width: 0;
        border-style: solid;
        padding-top: 11px;
        padding-right: 26px;
        padding-bottom: 11px;
        padding-left: 26px;
        border-radius: 2px;
        -webkit-flex-flow: row-reverse nowrap;
    }

    .brz .css-e2llf.css-e2llf:hover, .brz [data-css-e2llf][data-css-e2llf]:hover {
        color: rgba(255, 255, 255, 1);
        border-color: rgba(35, 157, 219, 0);
        background-color: rgba(122, 110, 102, 0.54);
    }

    @media (max-width: 767px) {
        .brz .css-e2llf, .brz [data-css-e2llf] {
            font-size: 15px;
            font-weight: 300;
            line-height: 1.6;
            letter-spacing: 0px;
            padding-top: 11px;
            padding-right: 26px;
            padding-bottom: 11px;
            padding-left: 26px;
            border-radius: 2px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-ol0y3e, .brz [data-css-ol0y3e] {
            will-change: flex, max-width;
            flex: 1 1 31.6%;
            max-width: 31.6%;
            -webkit-flex: 1 1 31.6%;
        }
    }

    .css-cyi6gu, [data-css-cyi6gu] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 13px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    .css-cyi6gu > .brz-bg-content, [data-css-cyi6gu] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 67px;
        padding-right: 15px;
        padding-bottom: 65px;
        padding-left: 47px;
    }

    .css-cyi6gu > .brz-bg-media, [data-css-cyi6gu] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-cyi6gu > .brz-bg-media > .brz-bg-image, [data-css-cyi6gu] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
    }

    .css-cyi6gu > .brz-bg-media > .brz-bg-color, [data-css-cyi6gu] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(255, 183, 27, 1);
    }

    @media (min-width: 768px) {
        .brz .css-cyi6gu, .brz [data-css-cyi6gu] {
            align-items: flex-start;
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
        }
    }

    @media (max-width: 767px) {
        .css-cyi6gu, [data-css-cyi6gu] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-cyi6gu > .brz-bg-content, [data-css-cyi6gu] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 10px;
            padding-bottom: 0px;
            padding-left: 10px;
        }

        .css-cyi6gu > .brz-bg-media > .brz-bg-image, [data-css-cyi6gu] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
        }

        .css-cyi6gu > .brz-bg-media > .brz-bg-color, [data-css-cyi6gu] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(255, 183, 27, 1);
        }
    }

    .css-i9tzew, [data-css-i9tzew] {
        z-index: auto;
        margin-top: 16px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    @media (max-width: 767px) {
        .css-i9tzew, [data-css-i9tzew] {
            margin-top: 10px;
            margin-right: 0px;
            margin-bottom: 10px;
            margin-left: 0px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-1061yym, .brz [data-css-1061yym] {
            will-change: flex, max-width;
            flex: 1 1 68.4%;
            max-width: 68.4%;
            -webkit-flex: 1 1 68.4%;
        }
    }

    .css-1n6ueah, [data-css-1n6ueah] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 13px;
    }

    .css-1n6ueah > .brz-bg-content, [data-css-1n6ueah] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 67px;
        padding-right: 15px;
        padding-bottom: 5px;
        padding-left: 47px;
    }

    .css-1n6ueah > .brz-bg-media, [data-css-1n6ueah] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-1n6ueah > .brz-bg-media > .brz-bg-image, [data-css-1n6ueah] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/bc206f744377825d36f2c4bf833dc18292e18b15.jpeg);
        background-position: 50% 50%;
    }

    .css-1n6ueah > .brz-bg-media > .brz-bg-color, [data-css-1n6ueah] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (min-width: 768px) {
        .brz .css-1n6ueah, .brz [data-css-1n6ueah] {
            align-items: flex-start;
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
        }
    }

    @media (max-width: 767px) {
        .css-1n6ueah, [data-css-1n6ueah] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-1n6ueah > .brz-bg-content, [data-css-1n6ueah] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 10px;
            padding-bottom: 0px;
            padding-left: 10px;
        }

        .css-1n6ueah > .brz-bg-media > .brz-bg-image, [data-css-1n6ueah] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/bc206f744377825d36f2c4bf833dc18292e18b15.jpeg);
            background-position: 50% 50%;
        }

        .css-1n6ueah > .brz-bg-media > .brz-bg-color, [data-css-1n6ueah] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-uhsi1k, [data-css-uhsi1k] {
        
        padding-top: 22px;
        padding-bottom: 15px;
    }

    @media (max-width: 767px) {
        .css-uhsi1k, [data-css-uhsi1k] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-19rf22h, .brz [data-css-19rf22h] {
            will-change: flex, max-width;
            flex: 1 1 100%;
            max-width: 100%;
            -webkit-flex: 1 1 100%;
        }
    }

    .css-1e3wxl3, [data-css-1e3wxl3] {
        
        padding-top: 15px;
        padding-bottom: 25px;
    }

    @media (max-width: 767px) {
        .css-1e3wxl3, [data-css-1e3wxl3] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    .brz .css-1063fyz, .brz [data-css-1063fyz] {
        font-family: Lato, sans-serif;
        font-size: 18px;
        line-height: 1.5;
        font-weight: 400;
        letter-spacing: 0;
    }

    .brz .css-1063fyz .brz-tabs__nav, .brz [data-css-1063fyz] .brz-tabs__nav {
        justify-content: center;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
    }

    .brz .css-1063fyz .brz-tabs__item--content, .brz [data-css-1063fyz] .brz-tabs__item--content {
        padding-top: 0px;
        padding-right: 10px;
        padding-bottom: 0px;
        padding-left: 10px;
    }

    .brz .css-1063fyz .brz-tabs__nav--button, .brz [data-css-1063fyz] .brz-tabs__nav--button, .brz .css-1063fyz .brz-tabs__items, .brz [data-css-1063fyz] .brz-tabs__items {
        color: rgba(0, 0, 0, 0.7);
        background-color: rgba(255, 255, 255, 1);
        border-color: rgba(255, 255, 255, 1);
        border-width: 2px;
    }

    .brz .css-1063fyz .brz-tabs__nav--button, .brz [data-css-1063fyz] .brz-tabs__nav--button {
        border-bottom-color: transparent;
    }

    .brz .css-1063fyz .brz-tabs__nav--active .brz-tabs__nav--button, .brz [data-css-1063fyz] .brz-tabs__nav--active .brz-tabs__nav--button {
        border-bottom-color: rgba(255, 255, 255, 1);
    }

    .brz .css-1063fyz .brz-tabs__nav--active::after, .brz [data-css-1063fyz] .brz-tabs__nav--active::after, .brz .css-1063fyz .brz-tabs__nav--active::before, .brz [data-css-1063fyz] .brz-tabs__nav--active::before {
        background-color: rgba(255, 255, 255, 1);
    }

    .brz .css-1063fyz .brz-tabs__nav--active::after, .brz [data-css-1063fyz] .brz-tabs__nav--active::after {
        right: calc(-100vw + 2px);
    }

    .brz .css-1063fyz .brz-tabs__nav--active::before, .brz [data-css-1063fyz] .brz-tabs__nav--active::before {
        left: calc(-100vw + 2px);
    }

    @media (max-width: 767px) {
        .brz .css-1063fyz, .brz [data-css-1063fyz] {
            font-size: 14px;
            line-height: 1.5;
            font-weight: 400;
            letter-spacing: 0px;
        }

        .brz .css-1063fyz .brz-tabs__nav, .brz [data-css-1063fyz] .brz-tabs__nav {
            justify-content: center;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
        }

        .brz .css-1063fyz .brz-tabs__nav--button, .brz [data-css-1063fyz] .brz-tabs__nav--button {
            border-bottom-color: rgba(255, 255, 255, 1);
            border-width: 0;
        }

        .brz .css-1063fyz .brz-tabs__nav--mobile--active .brz-tabs__nav--button, .brz [data-css-1063fyz] .brz-tabs__nav--mobile--active .brz-tabs__nav--button {
            border-bottom-width: 2px;
        }

        .brz .css-1063fyz .brz-tabs__item--content, .brz [data-css-1063fyz] .brz-tabs__item--content {
            padding-top: 0px;
            padding-right: 10px;
            padding-bottom: 0px;
            padding-left: 10px;
        }
    }

    .css-19s7pht, [data-css-19s7pht] {
        justify-content: center;
        padding-top: 90px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
    }

    @media (max-width: 767px) {
        .css-19s7pht, [data-css-19s7pht] {
            justify-content: center;
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
        }
    }

    .css-toohv1, [data-css-toohv1] {
        justify-content: center;
        padding-top: 0px;
        padding-right: 100px;
        padding-bottom: 0px;
        padding-left: 100px;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
    }

    @media (max-width: 767px) {
        .css-toohv1, [data-css-toohv1] {
            justify-content: center;
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
        }
    }

    .brz .css-15vm4k4, .brz [data-css-15vm4k4] {
        flex-flow: row-reverse nowrap;
        font-family: Lato, sans-serif;
        font-size: 14px;
        line-height: 1.1;
        font-weight: 400;
        letter-spacing: 0;
        color: rgba(255, 255, 255, 1);
        border-color: rgba(35, 157, 219, 0);
        background-color: rgba(122, 110, 102, 1);
        border-width: 0;
        border-style: solid;
        padding-top: 11px;
        padding-right: 26px;
        padding-bottom: 11px;
        padding-left: 26px;
        border-radius: 2px;
        -webkit-flex-flow: row-reverse nowrap;
    }

    .brz .css-15vm4k4.css-15vm4k4:hover, .brz [data-css-15vm4k4][data-css-15vm4k4]:hover {
        color: rgba(255, 255, 255, 1);
        border-color: rgba(35, 157, 219, 0);
        background-color: rgba(122, 110, 102, 0.88);
    }

    @media (max-width: 767px) {
        .brz .css-15vm4k4, .brz [data-css-15vm4k4] {
            font-size: 15px;
            font-weight: 300;
            line-height: 1.6;
            letter-spacing: 0px;
            padding-top: 11px;
            padding-right: 26px;
            padding-bottom: 11px;
            padding-left: 26px;
            border-radius: 2px;
        }
    }

    .brz .css-1jc60k4, .brz [data-css-1jc60k4] {
        font-size: 16px;
        margin-left: 10px;
        flex: 1 0 auto;
        stroke-width: 0;
        -webkit-flex: 1 0 auto;
    }

    .brz .css-1cpdk88, .brz [data-css-1cpdk88] {
        flex-flow: row-reverse nowrap;
        font-family: Montserrat, sans-serif;
        font-size: 12px;
        line-height: 1.8;
        font-weight: 600;
        letter-spacing: 3px;
        color: rgba(255, 255, 255, 1);
        border-color: rgba(35, 157, 219, 0);
        background-color: rgba(35, 157, 219, 1);
        border-width: 2px;
        border-style: solid;
        padding-top: 14px;
        padding-right: 42px;
        padding-bottom: 14px;
        padding-left: 42px;
        border-radius: 2px;
        -webkit-flex-flow: row-reverse nowrap;
    }

    .brz .css-1cpdk88.css-1cpdk88:hover, .brz [data-css-1cpdk88][data-css-1cpdk88]:hover {
        color: rgba(255, 255, 255, 1);
        border-color: rgba(35, 157, 219, 0);
        background-color: rgba(35, 157, 219, 0.8);
    }

    @media (max-width: 767px) {
        .brz .css-1cpdk88, .brz [data-css-1cpdk88] {
            font-size: 12px;
            font-weight: 600;
            line-height: 1.8;
            letter-spacing: 3px;
            padding-top: 11px;
            padding-right: 26px;
            padding-bottom: 11px;
            padding-left: 26px;
            border-radius: 2px;
        }
    }

    .css-1vys287, [data-css-1vys287] {
      
        padding-top: 33px;
        padding-bottom: 75px;
    }

    @media (max-width: 767px) {
        .css-1vys287, [data-css-1vys287] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    .css-719m6a, [data-css-719m6a] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 13px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    .css-719m6a > .brz-bg-content, [data-css-719m6a] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 47px;
    }

    .css-719m6a > .brz-bg-media, [data-css-719m6a] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-719m6a > .brz-bg-media > .brz-bg-image, [data-css-719m6a] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
    }

    .css-719m6a > .brz-bg-media > .brz-bg-color, [data-css-719m6a] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(255, 255, 255, 1);
    }

    @media (min-width: 768px) {
        .brz .css-719m6a, .brz [data-css-719m6a] {
            align-items: flex-start;
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
        }
    }

    @media (max-width: 767px) {
        .css-719m6a, [data-css-719m6a] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-719m6a > .brz-bg-content, [data-css-719m6a] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 10px;
            padding-bottom: 0px;
            padding-left: 10px;
        }

        .css-719m6a > .brz-bg-media > .brz-bg-image, [data-css-719m6a] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
        }

        .css-719m6a > .brz-bg-media > .brz-bg-color, [data-css-719m6a] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(255, 255, 255, 1);
        }
    }

    .css-1wa1wdi > .brz-ed-border, [data-css-1wa1wdi] > .brz-ed-border {
        flex: 1;
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        flex-direction: column;
        -webkit-flex: 1;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
    }

    @media (min-width: 768px) {
        .brz .css-1wa1wdi, .brz [data-css-1wa1wdi] {
            will-change: flex, max-width;
            flex: 1 1 68.4%;
            max-width: 68.4%;
            -webkit-flex: 1 1 68.4%;
        }
    }

    .css-r1x8zl, [data-css-r1x8zl] {
        z-index: auto;
    }

    .css-r1x8zl > .brz-bg-content, [data-css-r1x8zl] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
    }

    .css-r1x8zl > .brz-bg-media, [data-css-r1x8zl] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-r1x8zl > .brz-bg-media > .brz-bg-image, [data-css-r1x8zl] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/418af808f210f48c345e88cb6d7b63cdae9e5f35.jpeg);
        background-position: 50% 50%;
    }

    .css-r1x8zl > .brz-bg-media > .brz-bg-color, [data-css-r1x8zl] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (max-width: 767px) {
        .css-r1x8zl > .brz-bg-media > .brz-bg-image, [data-css-r1x8zl] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/418af808f210f48c345e88cb6d7b63cdae9e5f35.jpeg);
            background-position: 50% 50%;
        }

        .css-r1x8zl > .brz-bg-media > .brz-bg-color, [data-css-r1x8zl] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-1ln9jwx > .brz-bg-media, [data-css-1ln9jwx] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-1ln9jwx > .brz-bg-media > .brz-bg-image, [data-css-1ln9jwx] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
        background-attachment: scroll;
    }

    .css-1ln9jwx > .brz-bg-media > .brz-bg-color, [data-css-1ln9jwx] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(122, 110, 102, 1);
    }

    .css-1ln9jwx > .brz-bg-media > .brz-bg-map, [data-css-1ln9jwx] > .brz-bg-media > .brz-bg-map {
        display: none;
    }

    @media (max-width: 767px) {
        .css-1ln9jwx > .brz-bg-media > .brz-bg-image, [data-css-1ln9jwx] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
            background-attachment: scroll;
        }

        .css-1ln9jwx > .brz-bg-media > .brz-bg-color, [data-css-1ln9jwx] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(122, 110, 102, 1);
        }

        .css-1ln9jwx > .brz-bg-media > .brz-bg-map, [data-css-1ln9jwx] > .brz-bg-media > .brz-bg-map {
            display: none;
        }
    }

    .css-14tzvb4, [data-css-14tzvb4] {
   
        padding-top: 43px;
        padding-bottom: 42px;

    }

    @media (max-width: 767px) {
        .css-14tzvb4, [data-css-14tzvb4] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-1ivqk4u, .brz [data-css-1ivqk4u] {
            will-change: flex, max-width;
            flex: 1 1 25%;
            max-width: 25%;
            -webkit-flex: 1 1 25%;
        }
    }

    .brz .css-2akak2, .brz [data-css-2akak2] {
        max-width: 36%;
        height: auto;
        border-radius: 0px;
        opacity: 1;
        filter: brightness(100%) hue-rotate(0deg) saturate(100%);
        -webkit-filter: brightness(100%) hue-rotate(0deg) saturate(100%);
    }

    @media (max-width: 768px) {
        .brz .css-2akak2, .brz [data-css-2akak2] {
            max-width: 36%;
            height: auto;
            border-radius: 0px;
        }
    }

    .css-ttiawf > .brz-bg-media, [data-css-ttiawf] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(153, 114, 114, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-ttiawf > .brz-bg-media > .brz-bg-image, [data-css-ttiawf] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
        background-attachment: scroll;
    }

    .css-ttiawf > .brz-bg-media > .brz-bg-color, [data-css-ttiawf] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(255, 255, 255, 1);
    }

    .css-ttiawf > .brz-bg-media > .brz-bg-map, [data-css-ttiawf] > .brz-bg-media > .brz-bg-map {
        display: none;
    }

    @media (max-width: 767px) {
        .css-ttiawf > .brz-bg-media > .brz-bg-image, [data-css-ttiawf] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
            background-attachment: scroll;
        }

        .css-ttiawf > .brz-bg-media > .brz-bg-color, [data-css-ttiawf] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }

        .css-ttiawf > .brz-bg-media > .brz-bg-map, [data-css-ttiawf] > .brz-bg-media > .brz-bg-map {
            display: none;
        }
    }

    .css-1pzkdw1, [data-css-1pzkdw1] {
        max-width: 100%;
        padding-top: 0px;
        padding-bottom: 0px;
    }

    @media (max-width: 767px) {
        .css-1pzkdw1, [data-css-1pzkdw1] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-6w682k, .brz [data-css-6w682k] {
            will-change: flex, max-width;
            flex: 1 1 33.3%;
            max-width: 33.3%;
            -webkit-flex: 1 1 33.3%;
        }
    }

    .css-eug6ik, [data-css-eug6ik] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    .css-eug6ik > .brz-bg-content, [data-css-eug6ik] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
    }

    .css-eug6ik > .brz-bg-media, [data-css-eug6ik] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-eug6ik > .brz-bg-media > .brz-bg-image, [data-css-eug6ik] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/a10e31bff2b950e62fa2db24ec296d133a8135d0.jpeg);
        background-position: 50% 50%;
    }

    .css-eug6ik > .brz-bg-media > .brz-bg-color, [data-css-eug6ik] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(35, 158, 219, 0);
    }

    @media (min-width: 768px) {
        .brz .css-eug6ik, .brz [data-css-eug6ik] {
            align-items: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
        }
    }

    @media (max-width: 767px) {
        .css-eug6ik, [data-css-eug6ik] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-eug6ik > .brz-bg-content, [data-css-eug6ik] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 10px;
            padding-bottom: 0px;
            padding-left: 10px;
        }

        .css-eug6ik > .brz-bg-media > .brz-bg-image, [data-css-eug6ik] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/a10e31bff2b950e62fa2db24ec296d133a8135d0.jpeg);
            background-position: 50% 50%;
        }

        .css-eug6ik > .brz-bg-media > .brz-bg-color, [data-css-eug6ik] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .brz .css-15m1g5h, .brz [data-css-15m1g5h] {
        height: 450px;
    }

    @media (max-width: 767px) {
        .brz .css-15m1g5h, .brz [data-css-15m1g5h] {
            height: 450px;
        }
    }

    .css-2iv83d > .brz-ed-border, [data-css-2iv83d] > .brz-ed-border {
        flex: 1;
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        flex-direction: column;
        -webkit-flex: 1;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
    }

    @media (min-width: 768px) {
        .brz .css-2iv83d, .brz [data-css-2iv83d] {
            will-change: flex, max-width;
            flex: 1 1 33.3%;
            max-width: 33.3%;
            -webkit-flex: 1 1 33.3%;
        }
    }

    .css-1kdi9sb, [data-css-1kdi9sb] {
        z-index: auto;
    }

    .css-1kdi9sb > .brz-bg-content, [data-css-1kdi9sb] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
    }

    .css-1kdi9sb > .brz-bg-media, [data-css-1kdi9sb] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-1kdi9sb > .brz-bg-media > .brz-bg-image, [data-css-1kdi9sb] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/12baa086876d97660f61387ac68414899a8493c3.jpeg);
        background-position: 50% 50%;
    }

    .css-1kdi9sb > .brz-bg-media > .brz-bg-color, [data-css-1kdi9sb] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(35, 158, 219, 0);
    }

    @media (max-width: 767px) {
        .css-1kdi9sb > .brz-bg-media > .brz-bg-image, [data-css-1kdi9sb] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/12baa086876d97660f61387ac68414899a8493c3.jpeg);
            background-position: 50% 50%;
        }

        .css-1kdi9sb > .brz-bg-media > .brz-bg-color, [data-css-1kdi9sb] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-1wrc7dy > .brz-ed-border, [data-css-1wrc7dy] > .brz-ed-border {
        flex: 1;
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        flex-direction: column;
        -webkit-flex: 1;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
    }

    @media (min-width: 768px) {
        .brz .css-1wrc7dy, .brz [data-css-1wrc7dy] {
            will-change: flex, max-width;
            flex: 1 1 33.4%;
            max-width: 33.4%;
            -webkit-flex: 1 1 33.4%;
        }
    }

    .css-1tbzbvp, [data-css-1tbzbvp] {
        z-index: auto;
    }

    .css-1tbzbvp > .brz-bg-content, [data-css-1tbzbvp] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
    }

    .css-1tbzbvp > .brz-bg-media, [data-css-1tbzbvp] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-1tbzbvp > .brz-bg-media > .brz-bg-image, [data-css-1tbzbvp] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/2c76ae353cc07bfb7ebd7bd01a8b7aae89b2ccbf.jpeg);
        background-position: 50% 50%;
    }

    .css-1tbzbvp > .brz-bg-media > .brz-bg-color, [data-css-1tbzbvp] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(35, 158, 219, 0);
    }

    @media (max-width: 767px) {
        .css-1tbzbvp > .brz-bg-media > .brz-bg-image, [data-css-1tbzbvp] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/2c76ae353cc07bfb7ebd7bd01a8b7aae89b2ccbf.jpeg);
            background-position: 50% 50%;
        }

        .css-1tbzbvp > .brz-bg-media > .brz-bg-color, [data-css-1tbzbvp] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-1rh7mn3, [data-css-1rh7mn3] {
       
        padding-top: 75px;
        padding-bottom: 15px;
    }

    @media (max-width: 767px) {
        .css-1rh7mn3, [data-css-1rh7mn3] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-t360gx, .brz [data-css-t360gx] {
            will-change: flex, max-width;
            flex: 1 1 37.3%;
            max-width: 37.3%;
            -webkit-flex: 1 1 37.3%;
        }
    }

    .brz .css-cfty7q, .brz [data-css-cfty7q] {
        max-width: 100%;
        height: auto;
        border-radius: 0px;
        opacity: 1;
        filter: brightness(100%) hue-rotate(0deg) saturate(100%);
        -webkit-filter: brightness(100%) hue-rotate(0deg) saturate(100%);
    }

    @media (max-width: 768px) {
        .brz .css-cfty7q, .brz [data-css-cfty7q] {
            max-width: 100%;
            height: auto;
            border-radius: 0px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-1wo1sl0, .brz [data-css-1wo1sl0] {
            will-change: flex, max-width;
            flex: 1 1 62.7%;
            max-width: 62.7%;
            -webkit-flex: 1 1 62.7%;
        }
    }

    .css-1cs7zk9, [data-css-1cs7zk9] {
       
        padding-top: 15px;
        padding-bottom: 27px;
    }

    @media (max-width: 767px) {
        .css-1cs7zk9, [data-css-1cs7zk9] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    .css-14rabsn, [data-css-14rabsn] {
        padding-top: 10px;
        padding-right: 13%;
        padding-bottom: 10px;
        padding-left: 17%;
    }

    @media (max-width: 767px) {
        .css-14rabsn, [data-css-14rabsn] {
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
        }
    }

    .css-1tr7msa, [data-css-1tr7msa] {
     
        padding-top: 26px;
        padding-bottom: 15px;
    }

    @media (max-width: 767px) {
        .css-1tr7msa, [data-css-1tr7msa] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-u8ahs9, .brz [data-css-u8ahs9] {
            will-change: flex, max-width;
            flex: 1 1 59.4%;
            max-width: 59.4%;
            -webkit-flex: 1 1 59.4%;
        }
    }

    @media (min-width: 768px) {
        .brz .css-1b0mf8p, .brz [data-css-1b0mf8p] {
            will-change: flex, max-width;
            flex: 1 1 26%;
            max-width: 26%;
            -webkit-flex: 1 1 26%;
        }
    }

    @media (min-width: 768px) {
        .brz .css-19rgh32, .brz [data-css-19rgh32] {
            will-change: flex, max-width;
            flex: 1 1 74%;
            max-width: 74%;
            -webkit-flex: 1 1 74%;
        }
    }

    .css-9ezyjc > .brz-ed-border, [data-css-9ezyjc] > .brz-ed-border {
        flex: 1;
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        flex-direction: column;
        -webkit-flex: 1;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
    }

    @media (min-width: 768px) {
        .brz .css-9ezyjc, .brz [data-css-9ezyjc] {
            will-change: flex, max-width;
            flex: 1 1 40.6%;
            max-width: 40.6%;
            -webkit-flex: 1 1 40.6%;
        }
    }

    .css-ngjkti, [data-css-ngjkti] {
        z-index: auto;
    }

    .css-ngjkti > .brz-bg-content, [data-css-ngjkti] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
    }

    .css-ngjkti > .brz-bg-media, [data-css-ngjkti] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-ngjkti > .brz-bg-media > .brz-bg-image, [data-css-ngjkti] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
    }

    .css-ngjkti > .brz-bg-media > .brz-bg-color, [data-css-ngjkti] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (max-width: 767px) {
        .css-ngjkti > .brz-bg-media > .brz-bg-image, [data-css-ngjkti] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
        }

        .css-ngjkti > .brz-bg-media > .brz-bg-color, [data-css-ngjkti] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-43882k, [data-css-43882k] {
    
        padding-top: 37px;
        padding-bottom: 75px;
    }

    @media (max-width: 767px) {
        .css-43882k, [data-css-43882k] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    .css-85o7xj, [data-css-85o7xj] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    .css-85o7xj > .brz-bg-content, [data-css-85o7xj] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
    }

    .css-85o7xj > .brz-bg-media, [data-css-85o7xj] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-85o7xj > .brz-bg-media > .brz-bg-image, [data-css-85o7xj] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
    }

    .css-85o7xj > .brz-bg-media > .brz-bg-color, [data-css-85o7xj] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (min-width: 768px) {
        .brz .css-85o7xj, .brz [data-css-85o7xj] {
            align-items: flex-start;
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
        }
    }

    @media (max-width: 767px) {
        .css-85o7xj, [data-css-85o7xj] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-85o7xj > .brz-bg-content, [data-css-85o7xj] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
        }

        .css-85o7xj > .brz-bg-media > .brz-bg-image, [data-css-85o7xj] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
        }

        .css-85o7xj > .brz-bg-media > .brz-bg-color, [data-css-85o7xj] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-1v503i5 > .brz-ed-border, [data-css-1v503i5] > .brz-ed-border {
        flex: 1;
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        flex-direction: column;
        -webkit-flex: 1;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
    }

    @media (min-width: 768px) {
        .brz .css-1v503i5, .brz [data-css-1v503i5] {
            will-change: flex, max-width;
            flex: 1 1 50%;
            max-width: 50%;
            -webkit-flex: 1 1 50%;
        }
    }

    .css-1igfuj5, [data-css-1igfuj5] {
        z-index: auto;
    }

    .css-1igfuj5 > .brz-bg-content, [data-css-1igfuj5] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
    }

    .css-1igfuj5 > .brz-bg-media, [data-css-1igfuj5] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-1igfuj5 > .brz-bg-media > .brz-bg-image, [data-css-1igfuj5] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/993d346a5d6291ef70aab2d2b46864833d285e26.jpeg);
        background-position: 50% 50%;
    }

    .css-1igfuj5 > .brz-bg-media > .brz-bg-color, [data-css-1igfuj5] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (max-width: 767px) {
        .css-1igfuj5 > .brz-bg-media > .brz-bg-image, [data-css-1igfuj5] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/993d346a5d6291ef70aab2d2b46864833d285e26.jpeg);
            background-position: 50% 50%;
        }

        .css-1igfuj5 > .brz-bg-media > .brz-bg-color, [data-css-1igfuj5] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-11v7a3c, [data-css-11v7a3c] {
        z-index: auto;
    }

    .css-11v7a3c > .brz-bg-content, [data-css-11v7a3c] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
    }

    .css-11v7a3c > .brz-bg-media, [data-css-11v7a3c] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-11v7a3c > .brz-bg-media > .brz-bg-image, [data-css-11v7a3c] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/182307a6b17a6f3b1cd6b1a07c788c4a58d93564.jpeg);
        background-position: 50% 50%;
    }

    .css-11v7a3c > .brz-bg-media > .brz-bg-color, [data-css-11v7a3c] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (max-width: 767px) {
        .css-11v7a3c > .brz-bg-media > .brz-bg-image, [data-css-11v7a3c] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/182307a6b17a6f3b1cd6b1a07c788c4a58d93564.jpeg);
            background-position: 50% 50%;
        }

        .css-11v7a3c > .brz-bg-media > .brz-bg-color, [data-css-11v7a3c] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-17oy4xx, [data-css-17oy4xx] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    .css-17oy4xx > .brz-bg-content, [data-css-17oy4xx] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 5px;
        padding-right: 15px;
        padding-bottom: 5px;
        padding-left: 15px;
    }

    .css-17oy4xx > .brz-bg-media, [data-css-17oy4xx] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-17oy4xx > .brz-bg-media > .brz-bg-image, [data-css-17oy4xx] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
    }

    .css-17oy4xx > .brz-bg-media > .brz-bg-color, [data-css-17oy4xx] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(255, 183, 27, 1);
    }

    @media (min-width: 768px) {
        .brz .css-17oy4xx, .brz [data-css-17oy4xx] {
            align-items: flex-start;
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
        }
    }

    @media (max-width: 767px) {
        .css-17oy4xx, [data-css-17oy4xx] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-17oy4xx > .brz-bg-content, [data-css-17oy4xx] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 10px;
            padding-bottom: 0px;
            padding-left: 10px;
        }

        .css-17oy4xx > .brz-bg-media > .brz-bg-image, [data-css-17oy4xx] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
        }

        .css-17oy4xx > .brz-bg-media > .brz-bg-color, [data-css-17oy4xx] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(255, 183, 27, 1);
        }
    }

    .css-75d44z, [data-css-75d44z] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    .css-75d44z > .brz-bg-content, [data-css-75d44z] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 21px;
        padding-right: 21px;
        padding-bottom: 32px;
        padding-left: 21px;
    }

    .css-75d44z > .brz-bg-media, [data-css-75d44z] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-75d44z > .brz-bg-media > .brz-bg-image, [data-css-75d44z] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
    }

    .css-75d44z > .brz-bg-media > .brz-bg-color, [data-css-75d44z] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(122, 110, 102, 1);
    }

    @media (min-width: 768px) {
        .brz .css-75d44z, .brz [data-css-75d44z] {
            align-items: flex-start;
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
        }
    }

    @media (max-width: 767px) {
        .css-75d44z, [data-css-75d44z] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-75d44z > .brz-bg-content, [data-css-75d44z] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 10px;
            padding-bottom: 0px;
            padding-left: 10px;
        }

        .css-75d44z > .brz-bg-media > .brz-bg-image, [data-css-75d44z] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
        }

        .css-75d44z > .brz-bg-media > .brz-bg-color, [data-css-75d44z] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(122, 110, 102, 1);
        }
    }

    .css-1pn5kr5, [data-css-1pn5kr5] {
        z-index: auto;
    }

    .css-1pn5kr5 > .brz-bg-content, [data-css-1pn5kr5] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
    }

    .css-1pn5kr5 > .brz-bg-media, [data-css-1pn5kr5] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-1pn5kr5 > .brz-bg-media > .brz-bg-image, [data-css-1pn5kr5] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/745c61e23250ea38cb55cddf2d14ae3a1ce1c09a.jpeg);
        background-position: 50% 50%;
    }

    .css-1pn5kr5 > .brz-bg-media > .brz-bg-color, [data-css-1pn5kr5] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (max-width: 767px) {
        .css-1pn5kr5 > .brz-bg-media > .brz-bg-image, [data-css-1pn5kr5] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/745c61e23250ea38cb55cddf2d14ae3a1ce1c09a.jpeg);
            background-position: 50% 50%;
        }

        .css-1pn5kr5 > .brz-bg-media > .brz-bg-color, [data-css-1pn5kr5] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-15g4p86 > .brz-bg-media, [data-css-15g4p86] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-15g4p86 > .brz-bg-media > .brz-bg-image, [data-css-15g4p86] > .brz-bg-media > .brz-bg-image {
        background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/165d337efa933d9ea96ef057c6756e8a170f9b6d.jpeg);
        background-position: 50% 50%;
        background-attachment: scroll;
    }

    .css-15g4p86 > .brz-bg-media > .brz-bg-color, [data-css-15g4p86] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    .css-15g4p86 > .brz-bg-media > .brz-bg-map, [data-css-15g4p86] > .brz-bg-media > .brz-bg-map {
        display: none;
    }

    @media (max-width: 767px) {
        .css-15g4p86 > .brz-bg-media > .brz-bg-image, [data-css-15g4p86] > .brz-bg-media > .brz-bg-image {
            background-image: url(/wp-content/uploads/brizy/pages/1.0.41/2/iW=5000&iH=any/165d337efa933d9ea96ef057c6756e8a170f9b6d.jpeg);
            background-position: 50% 50%;
            background-attachment: scroll;
        }

        .css-15g4p86 > .brz-bg-media > .brz-bg-color, [data-css-15g4p86] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }

        .css-15g4p86 > .brz-bg-media > .brz-bg-map, [data-css-15g4p86] > .brz-bg-media > .brz-bg-map {
            display: none;
        }
    }

    .css-1cghdkq, [data-css-1cghdkq] {
      
        padding-top: 47px;
        padding-bottom: 57px;
    }

    @media (max-width: 767px) {
        .css-1cghdkq, [data-css-1cghdkq] {
            padding-top: 25px;
            padding-bottom: 25px;
        }
    }

    .css-n2fij5, [data-css-n2fij5] {
        padding-top: 10px;
        padding-right: 10px;
        padding-bottom: 0px;
        padding-left: 10px;
    }

    @media (max-width: 767px) {
        .css-n2fij5, [data-css-n2fij5] {
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
        }
    }

    .css-ntnmvj, [data-css-ntnmvj] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    .css-ntnmvj > .brz-bg-content, [data-css-ntnmvj] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 5px;
        padding-right: 15px;
        padding-bottom: 0px;
        padding-left: 15px;
    }

    .css-ntnmvj > .brz-bg-media, [data-css-ntnmvj] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-ntnmvj > .brz-bg-media > .brz-bg-image, [data-css-ntnmvj] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
    }

    .css-ntnmvj > .brz-bg-media > .brz-bg-color, [data-css-ntnmvj] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (min-width: 768px) {
        .brz .css-ntnmvj, .brz [data-css-ntnmvj] {
            align-items: flex-end;
            -webkit-box-align: end;
            -webkit-align-items: flex-end;
        }
    }

    @media (max-width: 767px) {
        .css-ntnmvj, [data-css-ntnmvj] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-ntnmvj > .brz-bg-content, [data-css-ntnmvj] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
        }

        .css-ntnmvj > .brz-bg-media > .brz-bg-image, [data-css-ntnmvj] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
        }

        .css-ntnmvj > .brz-bg-media > .brz-bg-color, [data-css-ntnmvj] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    .css-q2hqk1, [data-css-q2hqk1] {
        justify-content: flex-start;
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
        -webkit-box-pack: start;
        -webkit-justify-content: flex-start;
    }

    @media (max-width: 767px) {
        .css-q2hqk1, [data-css-q2hqk1] {
            justify-content: flex-start;
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
        }
    }

    .brz .css-3s6t0h, .brz [data-css-3s6t0h] {
        max-width: 16%;
        height: auto;
        border-radius: 0px;
        opacity: 1;
        filter: brightness(100%) hue-rotate(0deg) saturate(100%);
        -webkit-filter: brightness(100%) hue-rotate(0deg) saturate(100%);
    }

    @media (max-width: 768px) {
        .brz .css-3s6t0h, .brz [data-css-3s6t0h] {
            max-width: 16%;
            height: auto;
            border-radius: 0px;
        }
    }

    @media (min-width: 768px) {
        .brz .css-4py51v, .brz [data-css-4py51v] {
            will-change: flex, max-width;
            flex: 1 1 54.7%;
            max-width: 54.7%;
            -webkit-flex: 1 1 54.7%;
        }
    }

    @media (min-width: 768px) {
        .brz .css-azr7cd, .brz [data-css-azr7cd] {
            will-change: flex, max-width;
            flex: 1 1 45.3%;
            max-width: 45.3%;
            -webkit-flex: 1 1 45.3%;
        }
    }

    @media (min-width: 768px) {
        .brz .css-3q36jq, .brz [data-css-3q36jq] {
            will-change: flex, max-width;
            flex: 1 1 21.6%;
            max-width: 21.6%;
            -webkit-flex: 1 1 21.6%;
        }
    }

    @media (min-width: 768px) {
        .brz .css-t551vv, .brz [data-css-t551vv] {
            will-change: flex, max-width;
            flex: 1 1 78.4%;
            max-width: 78.4%;
            -webkit-flex: 1 1 78.4%;
        }
    }

    .css-1sifoqf, [data-css-1sifoqf] {
        justify-content: center;
        padding-top: 11px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
    }

    @media (max-width: 767px) {
        .css-1sifoqf, [data-css-1sifoqf] {
            justify-content: center;
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
        }
    }

    @media (min-width: 768px) {
        .brz .css-9k6jk5, .brz [data-css-9k6jk5] {
            will-change: flex, max-width;
            flex: 1 1 29.5%;
            max-width: 29.5%;
            -webkit-flex: 1 1 29.5%;
        }
    }

    .css-1hm60nl, [data-css-1hm60nl] {
        z-index: auto;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
    }

    .css-1hm60nl > .brz-bg-content, [data-css-1hm60nl] > .brz-bg-content {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: transparent;
        border-style: solid;
        padding-top: 5px;
        padding-right: 0px;
        padding-bottom: 5px;
        padding-left: 15px;
    }

    .css-1hm60nl > .brz-bg-media, [data-css-1hm60nl] > .brz-bg-media {
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 0px;
        border-left-width: 0px;
        border-color: rgba(102, 115, 141, 0);
        border-style: solid;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .css-1hm60nl > .brz-bg-media > .brz-bg-image, [data-css-1hm60nl] > .brz-bg-media > .brz-bg-image {
        background-image: none;
        background-position: 50% 50%;
    }

    .css-1hm60nl > .brz-bg-media > .brz-bg-color, [data-css-1hm60nl] > .brz-bg-media > .brz-bg-color {
        background-color: rgba(0, 0, 0, 0);
    }

    @media (min-width: 768px) {
        .brz .css-1hm60nl, .brz [data-css-1hm60nl] {
            align-items: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
        }
    }

    @media (max-width: 767px) {
        .css-1hm60nl, [data-css-1hm60nl] {
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .css-1hm60nl > .brz-bg-content, [data-css-1hm60nl] > .brz-bg-content {
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
        }

        .css-1hm60nl > .brz-bg-media > .brz-bg-image, [data-css-1hm60nl] > .brz-bg-media > .brz-bg-image {
            background-image: none;
            background-position: 50% 50%;
        }

        .css-1hm60nl > .brz-bg-media > .brz-bg-color, [data-css-1hm60nl] > .brz-bg-media > .brz-bg-color {
            background-color: rgba(0, 0, 0, 0);
        }
    }

    @media (min-width: 768px) {
        .brz .css-zoy3jq, .brz [data-css-zoy3jq] {
            will-change: flex, max-width;
            flex: 1 1 70.5%;
            max-width: 70.5%;
            -webkit-flex: 1 1 70.5%;
        }
    }

    .brz .css-zz85lb, .brz [data-css-zz85lb] {
        color: rgba(0, 0, 0, 1);
        border-color: rgba(252, 252, 252, 1);
        background-color: rgba(245, 245, 245, 1);
        border-width: 3px;
        border-style: solid;
        width: 28px;
        height: 28px;
        font-size: 18px;
        padding: 2px;
        border-radius: 3px;
        stroke-width: 0;
    }

    .brz .css-zz85lb:hover, .brz [data-css-zz85lb]:hover {
        color: rgba(81, 157, 195, 0.8);
        border-color: rgba(255, 255, 255, 1);
        background-color: rgba(255, 255, 255, 1);
    }

    @media (max-width: 767px) {
        .brz .css-zz85lb, .brz [data-css-zz85lb] {
            width: 64px;
            height: 64px;
            font-size: 18px;
            padding: 20px;
            border-radius: 6px;
            stroke-width: 0;
        }

    }
	</style>
<style>
<?php include "inlinecss.css"; ?>
</style>

    </head>

<body <?php body_class('brz-ed brz customize-support'); ?>>
	<header id="masthead" class="site-header">

		<div class="site-branding">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"> <i class="idnetit-orthodontie identit" style="font-size:75px;"></i>< </a>
		</div><!-- .site-branding -->

<nav id="site-navigation" class="main-navigation">
         <?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
           
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
    <header  class="headerMobile">
        <nav id='cssmenu'>
            <div class="logo"> 	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"> <img class="attachment-thumb" src="/wp-content/uploads/2018/07/Logo-identi-T.png" draggable="false" alt="Logo IdentiT" /> </a></div>
            <div id="head-mobile"></div>
            <div class="button"></div>
            <?php
            wp_nav_menu( array(
                'theme_location' => 'menu-1',
                'menu_id'        => 'primary-menu',
                "container"=>null
            ) );
            ?>
        </nav>
    </header>