<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package skull_rive
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php
        if ('post' === get_post_type()) :
            ?>
            <div class="entry-meta">
                <div class="dateArticle">
                    <div class="day font24"><?=get_the_date('d')?></div>
                    <div class="month font24"><?=get_the_date('M')?></div>
                </div>
           
                     <div class="titleArticle">
                              <h1 class="font30">  <?=get_the_title()?> </h1>
                              <p class='author font18 lato regular'>Publié par: IdentiT <span class="share">

                           
                        <a href="http://www.facebook.com/sharer.php?u=<?= get_permalink() ?>" target="_blank" class="font18 lato regular"><svg class="brz-icon-svg"><use xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/logo-fb-simple.svg#nc_icon"></use></svg></a>
                        <a href="https://twitter.com/intent/tweet/?url=<?= get_permalink() ?>" target="_blank" class="font18 lato regular"><svg class="brz-icon-svg"><use xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/logo-twitter.svg#nc_icon"></use></svg></a>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= get_permalink() ?>" target="_blank" class="font18 lato regular"><svg class="brz-icon-svg"><use xlink:href="/wp-content/uploads/brizy/editor/1.0.41/template/icons/glyph/logo-linkedin.svg#nc_icon"></use></svg></a>

                    </span></p>
                            </div>
                             </div><!-- .entry-meta -->
        <?php endif; ?>
    </header><!-- .entry-header -->
	<div class="singleImgArticle">
     <?= wp_get_attachment_image(get_post_thumbnail_id( ), "full") ?>
     </div>

    <div class="entry-content padding60y">
        <?php
        the_content();
		
        ?>
    </div><!-- .entry-content -->
    
    
</article><!-- #post-<?php the_ID(); ?> -->
