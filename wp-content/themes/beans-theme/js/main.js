jQuery(document).ready(function ($) {
    $.extend({
        replaceTag: function (currentElem, newTagObj, keepProps) {
            var $currentElem = $(currentElem);
            var i, $newTag = $(newTagObj).clone();
            if (keepProps) {//{{{
                newTag = $newTag[0];
                newTag.className = currentElem.className;
                $.extend(newTag.classList, currentElem.classList);
                $.extend(newTag.attributes, currentElem.attributes);
            }//}}}
            $currentElem.wrapAll($newTag);
            $currentElem.contents().unwrap();
            // return node; (Error spotted by Frank van Luijn)
            return this; // Suggested by ColeLawrence
        }
    });

    $.fn.extend({
        replaceTag: function (newTagObj, keepProps) {
            // "return" suggested by ColeLawrence
            return this.each(function() {
                jQuery.replaceTag(this, newTagObj, keepProps);
            });
        }
    });
    jQuery("div[class*='linkAdded_']:not(.vc_btn3-container)").each(function(){

        const regex = /linkAdded_([^ ]+)/gm;
        var classs=$(this).attr("class");
        classs=regex.exec(classs);
        console.log(classs[0],jQuery(".vc_btn3-container."+classs[0]).length,jQuery(".vc_btn3-container."+classs[0]+" a").attr("href"));
        if(classs&&jQuery(".vc_btn3-container."+classs[0]).length>0)
        {
            jQuery(this).replaceTag("<a>",true);
            jQuery("a."+classs[0]+":not(.vc_btn3-container)").attr("href",jQuery(".vc_btn3-container."+classs[0]+" a").attr("href"));
        }

    });
});

window.onscroll = function() {myFunction()};

var navbar = document.getElementById("site-navigation");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
}