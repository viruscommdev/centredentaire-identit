$ = jQuery;
place_changed=function(){};
autocomplete=null;
directionsDisplay=null;
directionsService=null;
$(document).ready(function () {
    Slider = $('.carousel.carousel-slider').carousel({fullWidth: true});

    function resizeHeight() {

        jQuery(document).resize(function () {
            listElement = {};
            jQuery("[class*='equalHeight']").each(function () {
                var regex = /equalHeight([a-z]+)/gm;
                var id = regex.exec(jQuery(this).attr("class"))[1];
                var height = Number(jQuery(this).height());
                if (typeof listElement[id] === "undefined")
                    listElement[id] = height;
                else if (listElement[id] < height)
                    listElement[id] = height;
            });
            for (id in listElement)
                jQuery(".equalHeight" + id).css({height: listElement[id] + "px"});
        }).resize();
        console.log("tirgger resizeHeight");
    };
    resizeHeight();
    jQuery(".addressToMap").hover(function () {
        id = jQuery(this).data("map");
        points = jQuery(this).data("points");
        try {
            var map = eval(id + "Map");
            if (typeof map !== "undefined" && map) {
                for (i in listMarkers) {
                    listMarkers[i].setAnimation(null);
                }
                listMarkers[points].setAnimation(google.maps.Animation.BOUNCE);
            }
        } catch (e) {
            console.log(e);
        }
    });
    listMarkers = {};

    function initialPin() {
        id = jQuery(".addressToMap:first").data("map");
        if (jQuery(".addressToMap:first").length > 0 && jQuery("#" + id).length > 0)
            var map = eval(id + "Map");
        if (typeof map !== "undefined" && map) {

            console.log("got the map");
            jQuery(".addressToMap").each(function () {
                id = jQuery(this).data("map");
                points = jQuery(this).data("points");
                var map = eval(id + "Map");
                pointsArray = points.split(",");
                listMarkers[points] = new google.maps.Marker({
                    position: new google.maps.LatLng(Number(pointsArray[0]), Number(pointsArray[1])),
                    map: map,
                    icon: jQuery("#" + id).data("pin")
                });
            });
            var bounds = new google.maps.LatLngBounds();
            for (i in listMarkers) {
                bounds.extend(listMarkers[i].getPosition());
            }
            map.fitBounds(bounds);
        } else {
            setTimeout(function () {
                initialPin();
            }, 1000);
        }
    }

    initialPin();

    function formSearchWindow() {

        listOptions = ["option 1", "option 2"];
        for (i in listOptions)
            jQuery(".windowHomepage select").append("<option value='" + i + "'>" + listOptions[i] + "</option>");
        jQuery(".windowHomepage .btn-theme").click(function () {
            url = "/trouver-un-professionnel/";
            document.location = url + "?address=" + jQuery(".windowHomepage input").val() + "&service=" + jQuery(".windowHomepage select").val();
        });
        var urlObj = new URL(document.URL);
        if (urlObj.searchParams.get("address"))
            jQuery(".windowHomepage input").val(urlObj.searchParams.get("address"));
        if (urlObj.searchParams.get("service"))
            jQuery(".windowHomepage select").val(urlObj.searchParams.get("service"));
        gMapReady(function () {
            geocoder = new google.maps.Geocoder();
            var address = jQuery(".windowHomepage input").val();
            if(address!==""){
                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == 'OK') {
                        position=results[0].geometry.location;
                        place_changed(position);
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }
        })
    }
    if(jQuery(".windowHomepage input").length>0){

        formSearchWindow();
        gMapReady(function () {
            var input = document.querySelectorAll(".windowHomepage input")[0];
            autocomplete = new google.maps.places.Autocomplete(input);

            directionsDisplay = new google.maps.DirectionsRenderer;
            directionsService = new google.maps.DirectionsService;

            function calculateAndDisplayRoute(directionsService, directionsDisplay, home, clinique) {
                var selectedMode = "WALKING";
                directionsService.route({
                    origin: home,  // Haight.
                    destination: clinique,  // Ocean Beach.
                    // Note that Javascript allows us to access the constant
                    // using square brackets and a string value as its
                    // "property."
                    travelMode: google.maps.TravelMode[selectedMode]
                }, function (response, status) {
                    if (status == 'OK') {
                        directionsDisplay.setDirections(response);
                    } else {
                        window.alert('Directions request failed due to ' + status);
                    }
                });
            }

            place_changed= function (positionHome) {

                if(typeof positionHome === "undefined"){

                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        // User entered the name of a Place that was not suggested and
                        // pressed the Enter key, or the Place Details request failed.
                        window.alert("No details available for input: '" + place.name + "'");
                        return;
                    }
                    positionHome =place.geometry.location;
                }

                id = jQuery(".addressToMap:first").data("map");
                if (jQuery(".addressToMap:first").length > 0 && jQuery("#" + id).length > 0)
                    var map = eval(id + "Map");
                else
                    return;
                directionsDisplay.setMap(map);
                var pointsArray = jQuery(".addressToMap:first").data("points").split(",");
                var clinique = new google.maps.LatLng(Number(pointsArray[0]), Number(pointsArray[1]));
                if (typeof map !== "undefined" && map && typeof listMarkers['home'] === "undefined")
                    listMarkers['home'] = new google.maps.Marker({
                        position: positionHome,
                        map: map
                    });
                else {
                    listMarkers['home'].setPosition(positionHome);
                }
                var service = new google.maps.DistanceMatrixService();
                var listCliniques = [];
                for (i in listMarkers) {
                    if (i !== "home") {
                        listCliniques.push(listMarkers[i].getPosition());
                    }
                }

                service.getDistanceMatrix(
                    {
                        origins: [listMarkers['home'].getPosition()],
                        destinations: listCliniques,
                        travelMode: 'DRIVING'
                    }, function (res, status) {
                        if (status === "OK") {
                            console.log("distances", res, status);
                            var cloestClinique = 0;
                            var cloestCliniquePos = null;
                            for (i in res.rows[0].elements) {
                                if (cloestClinique > res.rows[0].elements[i].distance.value || cloestClinique === 0) {
                                    cloestCliniquePos = listCliniques[i];
                                    cloestClinique = res.rows[0].elements[i].distance.value;
                                }
                            }

                            calculateAndDisplayRoute(directionsService, directionsDisplay, positionHome, cloestCliniquePos);
                        }
                    });


            }

            autocomplete.addListener('place_changed', place_changed);
        });
    }

});


addedMap = false;
MapReady = false;

function gMapReady(callback) {

    jQuery(document).scroll(function () {
        if (!addedMap && jQuery(document).scrollTop() > 100) {
            addedMap = true;
            jQuery.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyBiZUYaoYcd-tfJ4ZTnAO7pP-HYGanWnAM&libraries=places", function () {
                MapReady = true;
            });
        }
    });
    onceGMapReady(callback);
}

function onceGMapReady(callback) {
    if (MapReady)
        callback();
    else
        setTimeout(function () {
            onceGMapReady(callback)
        }, 300);
}