<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package skull_rive
 */

get_header();
?>
<div class="brz-root__container brz-reset-all">
<div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center   imgfullwidth">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="1366" height="421" src="/wp-content/uploads/2018/07/Groupe-292.png" class="vc_single_image-img attachment-full" alt="" srcset="/wp-content/uploads/2018/07/Groupe-292.png 1366w, /wp-content/uploads/2018/07/Groupe-292-300x92.png 300w, /wp-content/uploads/2018/07/Groupe-292-768x237.png 768w, /wp-content/uploads/2018/07/Groupe-292-1024x316.png 1024w" sizes="(max-width: 1366px) 100vw, 1366px"></div>
		</figure>
	</div>
</div></div></div></div>
    <div class="vc_row paddingContent padding130x padding60y articlePage">
        
        <div class="breadcrumb">  <?php
if ( function_exists('yoast_breadcrumb') ) {
  yoast_breadcrumb( '</p><p id="breadcrumbs">','</p><p>' );
}
?></div>
<div class="padding60yt"></div>
        <div class="vc_col-sm-8 ">
  
            <div id="primary" class="content-area padding40xr">
                <main id="main" class="site-main">

                    <?php
                    while (have_posts()) :
                        the_post();

                        get_template_part('template-parts/content', get_post_type());


                        // If comments are open or we have at least one comment, load up the comment template.
                        if (comments_open() || get_comments_number()) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                    ?>

                </main><!-- #main -->
            </div><!-- #primary -->
        </div>
        <div class="vc_col-sm-4">
            <?php $post_side = get_post(460);
   echo do_shortcode($post_side->post_content); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>