  <?php
get_header();
?>
   <?php
    ob_start();
    ?>
[vc_row][vc_column][vc_single_image image="469" img_size="full" alignment="center" el_class="imgfullwidth"][/vc_column][/vc_row]
[vc_row el_class="padding138x padding60yt"][vc_column width="3/4" el_class="padding40xr"][vsc_search_list][/vc_column][vc_column width="1/4"][vc_wp_search el_class="searchSidebar"][vc_wp_categories title="CATÉGORIES" options="count" el_class="catSidebar"][vsc_Blog_sidebar][/vc_column][/vc_row]
    <?php
    $str =ob_get_clean();
    echo do_shortcode($str);
    ?>

<?php get_footer(); ?>
