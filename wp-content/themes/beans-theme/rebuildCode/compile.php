<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 7/12/2018
 * Time: 8:53 AM
 */
function millisecondsphp() {
    $mt = explode(' ', microtime());
    return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
}
function recompilejson_html($file,$save_file){


    $http = new WP_Http();
    $options = array(
        'body' => array(
            'template_slug' => 'brizy',
            'template_version' => "1.0.41",
            'template_download_url' => "https://static.brizy.io/brizy/1.0.41/visual/export.js",
            'config_json' => json_encode(json_decode(file_get_contents(ABSPATH."wp-content/themes/beans-theme/rebuildCode/config.json"))), // ???
            'pages_json' => json_encode(array(
                array(
                    'id' => 1,
                    'data' => json_encode(json_decode(file_get_contents($file))),
                    'is_index' => true
                )
            )), // ???
            'globals_json' => json_encode(json_decode(file_get_contents(ABSPATH."wp-content/themes/beans-theme/rebuildCode/globals.json"))),
            'page_id' => 1
        ),
        "method" => "POST");

    $url = "http://editorcomplier-lb-1472504860.us-east-1.elb.amazonaws.com/compile";
    $wp_response = $http->request($url, $options);
    $data=json_decode( wp_remote_retrieve_body( $wp_response ), true );
    //TODO get name
    file_put_contents($save_file,json_encode($data));

}
add_action("wp_ajax_compile",function ()
{
    die();
});
if(isset($_POST['action'])&&$_POST['action']=="brizy_update_item"){
    $text=stripslashes($_POST['data']);
    $data=json_decode($text,true);
    foreach ($data["items"] as $index=>$section){
        if($section['value']['customClassName'])
            $name="_".$section['value']['customClassName'];
        else
            $name="_section".$index;
        file_put_contents(ABSPATH."wp-content/themes/beans-theme/vc_extend_data/".$_POST['slug'].$name.".json",json_encode(["items"=>[$section]]));
    }
}
if(isset($_GET['recompile'])&&$_GET['recompile']=="brizy"){
    $start=millisecondsphp();
    echo "start :".$start;
    foreach ($vc_module->getDirContents(ABSPATH.'wp-content/themes/beans-theme/vc_extend_data/') as $filename){
        if(strpos($filename,"_body.json")===false) {

            $pathinfo = pathinfo($filename);
            $bodyfile = ABSPATH . "wp-content/themes/beans-theme/vc_extend_data/" . $pathinfo["filename"] . "_body.json";
            if(!file_exists($bodyfile)){
                recompilejson_html($filename,$bodyfile);
            }
        }
    }

    echo "end :".millisecondsphp();
    echo "done".millisecondsphp()-$start;
    die("done");
}