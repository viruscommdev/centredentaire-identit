<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit8e18437953be1a0c96f6a5d7cbd5e5f6
{
    public static $prefixLengthsPsr4 = array (
        'g' => 
        array (
            'gymadarasz\\xparser\\tests\\' => 25,
            'gymadarasz\\xparser\\' => 19,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'gymadarasz\\xparser\\tests\\' => 
        array (
            0 => __DIR__ . '/..' . '/gymadarasz/xparser/tests',
        ),
        'gymadarasz\\xparser\\' => 
        array (
            0 => __DIR__ . '/..' . '/gymadarasz/xparser',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit8e18437953be1a0c96f6a5d7cbd5e5f6::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit8e18437953be1a0c96f6a5d7cbd5e5f6::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
