<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package skull_rive
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">


	<?php
	 comment_form([
			"fields"=>[
					'author' => '<p class="comment-form-author">'  .
						'<input id="author" name="author" type="text" placeholder="'. __( 'Name' ) .'*" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="100"' . $aria_req . $html_req . ' /></p>',
					'email'  => '<p class="comment-form-email">'.
						'<input id="email" name="email" placeholder="'. __( 'Email' ) .'*" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes"' . $aria_req . $html_req  . ' /></p>'
		
				],
            "class_submit"=>"btn-theme",
			"comment_field"=>'<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" placeholder="' . _x( 'Commentaire' ) .'*" aria-required="true"></textarea></p>'
        
    ]);
	
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h5 class="comments-title">
			<?php
			$skull_rive_comment_count = get_comments_number();
			if ( '1' === $skull_rive_comment_count ) {
				printf(
					/* translators: 1: title. */
					esc_html__( 'One thought on &ldquo;%1$s&rdquo;', 'twentyfifteen' ),
					'<span>' . get_the_title() . '</span>'
				);
			} else {
				printf( // WPCS: XSS OK.
					/* translators: 1: comment count number, 2: title. */
					esc_html__( '%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $skull_rive_comment_count, 'comments title', 'beans-theme' ) ,
					number_format_i18n( $skull_rive_comment_count ),
					'<span>' . get_the_title() . '</span>'
				);
			}
			?>
		</h5><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
			wp_list_comments( array(
				'style'      => 'ol',
				'short_ping' => true,
			) );
			?>
		</ol><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'beans-theme'); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().

   
	?>

</div><!-- #comments -->
