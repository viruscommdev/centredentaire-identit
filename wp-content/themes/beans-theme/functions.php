<?php
/**
 * Beans Framework.
 * This core file should only be overwritten via your child theme.
 *
 * We strongly recommend to read the Beans documentation to find out more about
 * how to customize the Beans theme.
 *
 * @author Beans
 * @link   https://www.getbeans.io
 * @package Beans\Framework
 */

/**
 * Initialize Beans theme framework.
 *
 * @author Beans
 * @link   https://www.getbeans.io
 */

	// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => 'Primary',
			"lang_menu" => 'Language menu'
		) );
require_once dirname( __FILE__ ) . '/lib/init.php';
/**
 * Enqueue scripts and styles.
 */
function identiT_scripts() {
    wp_enqueue_script( 'identiT-menuMobile', get_template_directory_uri() . '/js/menuMobile.js', array("jquery"), '20151215', true );
    wp_enqueue_script( 'identiT-mainjs', get_template_directory_uri() . '/js/main.js', array("jquery"), '20151215', true );
 wp_enqueue_script( 'identiT-brizyjs', get_template_directory_uri() . '/js/brizy.js', array("jquery"), '20151215', true );

    wp_enqueue_style("theme","/wp-content/themes/beans-theme/style.css");
	
    wp_enqueue_style("birzy",get_template_directory_uri() . '/css/brizy.css');
}
add_action('wp_footer',function(){
	
    wp_enqueue_style("latogoogle","https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i");
    wp_enqueue_style("fontawsome","https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
});
add_action( 'wp_enqueue_scripts', 'identiT_scripts' );

//wp_enqueue_script("editorCompiler","/wp-content/themes/beans-theme/rebuildCode/editorCompiler.js",["jquery"]);


include_once "integrated_vc.php";
include_once "rebuildCode/compile.php";
include_once "add_last_date.php";