<?php
/*
Template Name: custom-page
*/
get_header();
?>
    <div class="brz-root__container brz-reset-all">
        <?php
        $post_content = get_post(get_the_ID());
        echo do_shortcode($post_content->post_content);
        ?>

    </div>
<?php get_footer(); ?>