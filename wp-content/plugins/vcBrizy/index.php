<?php
/**
 * Plugin Name: visual composer + brizy
 * Plugin URI: https://visualcomposer.io/
 * Description: editor backend base on brizy
 * Version: 1.1
 * Author: visual composer
 * Author URI: https://visualcomposer.io/
 * License: GPL2+
 */
class pluginVCB{
    function __construct()
    {
        $this->wp_ajax_call_data();
        $this->addScriptandStyle();
    }
    function wp_ajax_call_data(){
        add_action('wp_ajax_call_data_vcb',function (){
            if($this->get_param("id"))
            {
                header("Content-Type: application/json");
                echo json_encode(get_post_meta($this->get_param("id"),"brizy"));
            }
            die("");
        });
    }
    function addScriptandStyle(){
        add_action('admin_enqueue_scripts',function(){
           wp_enqueue_style('style_vcb',plugins_url('style_vcb.css',__FILE__));
           wp_enqueue_script('script_vcb',plugins_url('script_vcb.js',__FILE__));
        });
    }
    function get_param($key){
        if(isset($_GET[$key]))
            return $_GET[$key];
        return null;
    }
}
new pluginVCB();